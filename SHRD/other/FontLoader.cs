﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Text;

namespace SHRD
{
    class FontLoader
    {
        PrivateFontCollection BasicFont;

        public FontLoader()
        {
            BasicFont = new PrivateFontCollection();
            BasicFont.AddFontFile("16872.ttf");
        }

        public Font Load()
        {
            return new Font(BasicFont.Families[0], 24);
        }


    }
}
