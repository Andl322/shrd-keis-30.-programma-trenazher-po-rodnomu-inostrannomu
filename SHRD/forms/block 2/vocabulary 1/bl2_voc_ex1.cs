﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SHRD
{
    class bl2_voc_ex1
    {
        struct Answer
        {
            public TextBox element;
            public string answer;

            public Answer(TextBox tb, string text)
            {
                this.element = tb;
                this.answer = text;
            }
        }

        Button next;
        Button listen;
        Button menu;
       
        PictureBox exercises;

        Form1 mw;
        FontLoader fl = new FontLoader();
        List<Answer> answers = new List<Answer>();

        public bl2_voc_ex1(Form1 form)
        {
            ////////////загрузка основного


            mw = form;

            mw.SuspendLayout();
            /////////////////////////////


            //Фоновая картинка
            exercises = new PictureBox();
            exercises.Image = global::SHRD.Properties.Resources.vocabulary_bl2_1;
            exercises.Location = new Point(0, 0);
            exercises.Name = "exercises";
            exercises.TabIndex = 0;
            exercises.TabStop = false;
            exercises.Visible = true;
            exercises.Dock = DockStyle.Fill;
            exercises.SizeMode = PictureBoxSizeMode.StretchImage;
            //////

            //menu
            menu = new Button();
            menu.Location = new Point(295, 587);
            menu.Size = new Size(210, 66);
            menu.BackColor = Color.FromArgb(61, 155, 153);
            menu.Font = fl.Load();
            menu.Text = "Menu";
            menu.Click += new EventHandler(menu_click);

            //listen
            listen = new Button();
            listen.Location = new Point(510, 587);
            listen.Size = new Size(210, 66);
            listen.BackColor = Color.FromArgb(61, 155, 153);
            listen.Font = fl.Load();
            listen.Text = "Listen";
            listen.Click += new EventHandler(listen_click);

            //next
            next = new Button();
            next.Location = new Point(725, 587);
            next.Size = new Size(210, 66);
            next.BackColor = Color.FromArgb(61, 155, 153);
            next.Font = fl.Load();
            next.Text = "Next>>>";
            next.Click += new EventHandler(next_click);

            

            mw.ResumeLayout();

            
        }

      
        

        public void AddElements()
        {



           
            mw.Controls.Add(menu);
            mw.Controls.Add(next);
            mw.Controls.Add(listen);
            mw.Controls.Add(exercises);


        }

        public void RemoveElements()
        {

            mw.Controls.Remove(menu);
            mw.Controls.Remove(next);
            mw.Controls.Remove(listen);
            mw.Controls.Remove(exercises);
        }

        void next_click(object sender, EventArgs e)
        {
            bl2_voc_ex2 bl2vocex2 = new bl2_voc_ex2(mw);
            RemoveElements();
            bl2vocex2.AddElements();
            player.Stop();
        }

        //audio 
        System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"media\bl2\bl2_voc_ex1.wav");

        void listen_click(object sender, EventArgs e)
        {
            
            //поменять звук на правильный

            //прикрутить таймер надо для текста кнопки, чтобы listen на stop менялось, а то я так и не понял

            player.Play();

        }

        void menu_click(object sender, EventArgs e)
        {
            bl2_voc_exes bl2vocexes = new bl2_voc_exes(mw);
            RemoveElements();
            bl2vocexes.AddElements();
            player.Stop();
        }
    }
}