﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SHRD
{
    class bl2_voc_ex2
    {
        struct Answer
        {
            public TextBox element;
            public string answer;

            public Answer(TextBox tb, string text)
            {
                this.element = tb;
                this.answer = text;
            }
        }

        Button back;
        Button check;
        Button menu;
        Button listen;
        TextBox textBox1;
        TextBox textBox2;
        TextBox textBox3;
        TextBox textBox4;
        TextBox textBox5;
        TextBox textBox6;
        TextBox textBox7;
        TextBox textBox8;
        PictureBox exercises;

        Form1 mw;
        FontLoader fl = new FontLoader();
        List<Answer> answers = new List<Answer>();

        public bl2_voc_ex2(Form1 form)
        {
            ////////////загрузка основного


            mw = form;

            mw.SuspendLayout();
            /////////////////////////////


            //Фоновая картинка
            exercises = new PictureBox();
            exercises.Image = global::SHRD.Properties.Resources.vocabulary_bl2_2;
            exercises.Location = new Point(0, 0);
            exercises.Name = "exercises";
            exercises.TabIndex = 0;
            exercises.TabStop = false;
            exercises.Visible = true;
            exercises.Dock = DockStyle.Fill;
            exercises.SizeMode = PictureBoxSizeMode.StretchImage;
            //////

            //menu
            menu = new Button();
            menu.Location = new Point(845, 587);
            menu.Size = new Size(210, 66);
            menu.BackColor = Color.FromArgb(61, 155, 153);
            menu.Font = fl.Load();
            menu.Text = "Next>>>";
            menu.Click += new EventHandler(menu_click);

            //back
            back = new Button();
            back.Location = new Point(200, 587);
            back.Size = new Size(210, 66);
            back.BackColor = Color.FromArgb(61, 155, 153);
            back.Font = fl.Load();
            back.Text = "<<<Back";
            back.Click += new EventHandler(back_click);

            //check
            check = new Button();
            check.Location = new Point(630, 587);
            check.Size = new Size(210, 66);
            check.BackColor = Color.FromArgb(61, 155, 153);
            check.Font = fl.Load();
            check.Text = "Check";
            check.Click += new EventHandler(check_click);

            //listen
            listen = new Button();
            listen.Location = new Point(415, 587);
            listen.Size = new Size(210, 66);
            listen.BackColor = Color.FromArgb(61, 155, 153);
            listen.Font = fl.Load();
            listen.Text = "listen";
            listen.Click += new EventHandler(listen_click);

            /// 1
            textBox1 = new TextBox();
            textBox1.Location = new Point(270, 295);
            textBox1.Name = "textBox1";
            textBox1.Font = new Font("", 15F);
            textBox1.Size = new Size(160, 70);
            textBox1.BorderStyle = BorderStyle.None;
            textBox1.TextAlign = HorizontalAlignment.Center;

            //2
            textBox2 = new TextBox();
            textBox2.Location = new Point(465, 295);
            textBox2.Name = "textBox2";
            textBox2.Font = new Font("", 15F);
            textBox2.Size = new Size(160, 90);
            textBox2.BorderStyle = BorderStyle.None;
            textBox2.TextAlign = HorizontalAlignment.Center;


            //3
            textBox3 = new TextBox();
            textBox3.Location = new Point(660, 295);
            textBox3.Name = "textBox3";
            textBox3.Font = new Font("", 15F);
            textBox3.Size = new Size(160, 90);
            textBox3.BorderStyle = BorderStyle.None;
            textBox3.TextAlign = HorizontalAlignment.Center;

            //4
            textBox4 = new TextBox();
            textBox4.Location = new Point(855, 295);
            textBox4.Name = "textBox4";
            textBox4.Font = new Font("", 15F);
            textBox4.Size = new Size(160, 90);
            textBox4.BorderStyle = BorderStyle.None;
            textBox4.TextAlign = HorizontalAlignment.Center;

            //5
            textBox5 = new TextBox();
            textBox5.Location = new Point(270, 430);
            textBox5.Name = "textBox5";
            textBox5.Font = new Font("", 15F);
            textBox5.Size = new Size(160, 90);
            textBox5.BorderStyle = BorderStyle.None;
            textBox5.TextAlign = HorizontalAlignment.Center;

            //6
            textBox6 = new TextBox();
            textBox6.Location = new Point(465, 430);
            textBox6.Name = "textBox6";
            textBox6.Font = new Font("", 15F);
            textBox6.Size = new Size(160, 90);
            textBox6.BorderStyle = BorderStyle.None;
            textBox6.TextAlign = HorizontalAlignment.Center;

            //7
            textBox7 = new TextBox();
            textBox7.Location = new Point(660, 430);
            textBox7.Name = "textBox7";
            textBox7.Font = new Font("", 15F);
            textBox7.Size = new Size(160, 90);
            textBox7.BorderStyle = BorderStyle.None;
            textBox7.TextAlign = HorizontalAlignment.Center;

            //8
            textBox8 = new TextBox();
            textBox8.Location = new Point(855, 430);
            textBox8.Name = "textBox8";
            textBox8.Font = new Font("", 15F);
            textBox8.Size = new Size(160, 90);
            textBox8.BorderStyle = BorderStyle.None;
            textBox8.TextAlign = HorizontalAlignment.Center;




            mw.ResumeLayout();

            FillAnswerTable();
        }

        void FillAnswerTable()
        {
            if (answers.Count != 0)
            {
                answers.Clear();
            }

            answers.Add(new Answer(textBox1, "ferry"));
            answers.Add(new Answer(textBox2, "trolley"));
            answers.Add(new Answer(textBox3, "flight"));
            answers.Add(new Answer(textBox4, "delay"));
            answers.Add(new Answer(textBox5, "customs"));
            answers.Add(new Answer(textBox6, "passport"));
            answers.Add(new Answer(textBox7, "plane"));
            answers.Add(new Answer(textBox8, "luggage"));


        }

        public void AddElements()
        {

            mw.Controls.Add(textBox1);
            mw.Controls.Add(textBox2);
            mw.Controls.Add(textBox3);
            mw.Controls.Add(textBox4);
            mw.Controls.Add(textBox5);
            mw.Controls.Add(textBox6);
            mw.Controls.Add(textBox7);
            mw.Controls.Add(textBox8);
            mw.Controls.Add(listen);
            mw.Controls.Add(menu);
            mw.Controls.Add(back);
            mw.Controls.Add(check);
            mw.Controls.Add(exercises);


        }

        public void RemoveElements()
        {
            mw.Controls.Remove(textBox1);
            mw.Controls.Remove(textBox2);
            mw.Controls.Remove(textBox3);
            mw.Controls.Remove(textBox4);
            mw.Controls.Remove(textBox5);
            mw.Controls.Remove(textBox6);
            mw.Controls.Remove(textBox7);
            mw.Controls.Remove(textBox8);
            mw.Controls.Remove(listen);
            mw.Controls.Remove(menu);
            mw.Controls.Remove(back);
            mw.Controls.Remove(check);
            mw.Controls.Remove(exercises);
        }

        void back_click(object sender, EventArgs e)
        {
            bl2_voc_ex1 bl2vocex1 = new bl2_voc_ex1(mw);
            RemoveElements();
            bl2vocex1.AddElements();
            player.Stop();
        }

        System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"media\bl2\bl2_voc_ex2.wav");

        void listen_click(object sender, EventArgs e)
        {
            //поменять звук на правильный

            //прикрутить таймер надо для текста кнопки, чтобы listen на stop менялось, а то я так и не понял

            player.Play();
        }

        void check_click(object sender, EventArgs e)
        {
            foreach (var a in answers)
            {
                if (a.element.Text != a.answer)
                {
                    a.element.ForeColor = Color.Red;
                }
                else if(a.element.Text == " ")
                {
                    a.element.ForeColor = Color.Black;
                }
                            

                
                else
                {
                    a.element.ForeColor = Color.Green;
                }
            }
        }

        void menu_click(object sender, EventArgs e)
        {
            bl2_voc_ex3 bl2vocex3 = new bl2_voc_ex3(mw);
            RemoveElements();
            bl2vocex3.AddElements();
            player.Stop();
        }
    }
}
