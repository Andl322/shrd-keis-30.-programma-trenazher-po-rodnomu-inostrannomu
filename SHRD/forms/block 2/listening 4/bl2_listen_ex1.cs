﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SHRD
{
    class bl2_listen_ex1
    {
        struct Answer
        {
            public TextBox element;
            public string answer;

            public Answer(TextBox tb, string text)
            {
                this.element = tb;
                this.answer = text;
            }
        }

        Button back;
        Button check;
        Button menu;
        Button listen;
        TextBox textBox1;
        TextBox textBox2;
        TextBox textBox3;
        TextBox textBox4;
        PictureBox exercises;

        Form1 mw;
        FontLoader fl = new FontLoader();
        List<Answer> answers = new List<Answer>();

        public bl2_listen_ex1(Form1 form)
        {
            ////////////загрузка основного


            mw = form;

            mw.SuspendLayout();
            /////////////////////////////


            //Фоновая картинка
            exercises = new PictureBox();
            exercises.Image = global::SHRD.Properties.Resources.listen_bl2_1;
            exercises.Location = new Point(0, 0);
            exercises.Name = "exercises";
            exercises.TabIndex = 0;
            exercises.TabStop = false;
            exercises.Visible = true;
            exercises.Dock = DockStyle.Fill;
            exercises.SizeMode = PictureBoxSizeMode.StretchImage;
            //////

            //menu
            menu = new Button();
            menu.Location = new Point(845, 587);
            menu.Size = new Size(210, 66);
            menu.BackColor = Color.FromArgb(61, 155, 153);
            menu.Font = fl.Load();
            menu.Text = "Finish";
            menu.Click += new EventHandler(menu_click);

            //back
            back = new Button();
            back.Location = new Point(200, 587);
            back.Size = new Size(210, 66);
            back.BackColor = Color.FromArgb(61, 155, 153);
            back.Font = fl.Load();
            back.Text = "Menu";
            back.Click += new EventHandler(back_click);

            //check
            check = new Button();
            check.Location = new Point(630, 587);
            check.Size = new Size(210, 66);
            check.BackColor = Color.FromArgb(61, 155, 153);
            check.Font = fl.Load();
            check.Text = "Check";
            check.Click += new EventHandler(check_click);

            //listen
            listen = new Button();
            listen.Location = new Point(415, 587);
            listen.Size = new Size(210, 66);
            listen.BackColor = Color.FromArgb(61, 155, 153);
            listen.Font = fl.Load();
            listen.Text = "listen";
            listen.Click += new EventHandler(listen_click);

            /// 1
            textBox1 = new TextBox();
            textBox1.Location = new Point(480, 307);
            textBox1.Name = "textBox1";
            textBox1.Font = new Font("", 15F);
            textBox1.Size = new Size(20, 70);
            textBox1.BorderStyle = BorderStyle.None;
            textBox1.TextAlign = HorizontalAlignment.Center;

            //2
            textBox2 = new TextBox();
            textBox2.Location = new Point(755, 307);
            textBox2.Name = "textBox2";
            textBox2.Font = new Font("", 15F);
            textBox2.Size = new Size(20, 90);
            textBox2.BorderStyle = BorderStyle.None;
            textBox2.TextAlign = HorizontalAlignment.Center;


            //3
            textBox3 = new TextBox();
            textBox3.Location = new Point(470, 523);
            textBox3.Name = "textBox3";
            textBox3.Font = new Font("", 15F);
            textBox3.Size = new Size(20, 90);
            textBox3.BorderStyle = BorderStyle.None;
            textBox3.TextAlign = HorizontalAlignment.Center;

            //4
            textBox4 = new TextBox();
            textBox4.Location = new Point(750, 523);
            textBox4.Name = "textBox4";
            textBox4.Font = new Font("", 15F);
            textBox4.Size = new Size(20, 90);
            textBox4.BorderStyle = BorderStyle.None;
            textBox4.TextAlign = HorizontalAlignment.Center;

            



            mw.ResumeLayout();

            FillAnswerTable();
        }

        void FillAnswerTable()
        {
            if (answers.Count != 0)
            {
                answers.Clear();
            }

            answers.Add(new Answer(textBox1, "3"));
            answers.Add(new Answer(textBox2, "2"));
            answers.Add(new Answer(textBox3, "1"));
            answers.Add(new Answer(textBox4, "4"));


        }

        public void AddElements()
        {

            mw.Controls.Add(textBox1);
            mw.Controls.Add(textBox2);
            mw.Controls.Add(textBox3);
            mw.Controls.Add(textBox4);
            mw.Controls.Add(listen);
            mw.Controls.Add(menu);
            mw.Controls.Add(back);
            mw.Controls.Add(check);
            mw.Controls.Add(exercises);


        }

        public void RemoveElements()
        {
            mw.Controls.Remove(textBox1);
            mw.Controls.Remove(textBox2);
            mw.Controls.Remove(textBox3);
            mw.Controls.Remove(textBox4);
            mw.Controls.Remove(listen);
            mw.Controls.Remove(menu);
            mw.Controls.Remove(back);
            mw.Controls.Remove(check);
            mw.Controls.Remove(exercises);
        }

        void back_click(object sender, EventArgs e)
        {
            bl2_listen_exes bl2listenexes = new bl2_listen_exes(mw);
            RemoveElements();
            bl2listenexes.AddElements();
            player.Stop();
        }

        System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"media\bl2\bl2_listen_ex1.wav");

        void listen_click(object sender, EventArgs e)
        {
            //поменять звук на правильный

            //прикрутить таймер надо для текста кнопки, чтобы listen на stop менялось, а то я так и не понял

            player.Play();
        }

        void check_click(object sender, EventArgs e)
        {
            foreach (var a in answers)
            {
                if (a.element.Text != a.answer)
                {
                    a.element.ForeColor = Color.Red;
                }
                else
                {
                    a.element.ForeColor = Color.Green;
                }
            }
        }

        void menu_click(object sender, EventArgs e)
        {
            bl2_listen_exes bl2vocexes = new bl2_listen_exes(mw);
            RemoveElements();
            bl2vocexes.AddElements();
            player.Stop();
        }
    }
}
