﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SHRD
{
    class bl2_write_ex3
    {
        struct Answer
        {
            public TextBox element;
            public string answer;

            public Answer(TextBox tb, string text)
            {
                this.element = tb;
                this.answer = text;
            }
        }

        Button back;
        Button check;
        Button next;
        Button listen;
        TextBox textBox1;
        TextBox textBox2;
        TextBox textBox3;
        TextBox textBox4;
        TextBox textBox5;
        TextBox textBox6;
        TextBox textBox7;
        TextBox textBox8;
        TextBox textBox9;
        TextBox textBox10;
        TextBox textBox11;
        TextBox textBox12;
        TextBox textBox13;
        TextBox textBox14;
        TextBox textBox15;
        TextBox textBox16;
        PictureBox exercises;

        Form1 mw;
        FontLoader fl = new FontLoader();
        List<Answer> answers = new List<Answer>();

        public bl2_write_ex3(Form1 form)
        {
            ////////////загрузка основного


            mw = form;

            mw.SuspendLayout();
            /////////////////////////////


            //Фоновая картинка
            exercises = new PictureBox();
            exercises.Image = global::SHRD.Properties.Resources.bl2_writing_ex3;
            exercises.Location = new Point(0, 0);
            exercises.Name = "exercises";
            exercises.TabIndex = 0;
            exercises.TabStop = false;
            exercises.Visible = true;
            exercises.Dock = DockStyle.Fill;
            exercises.SizeMode = PictureBoxSizeMode.StretchImage;
            //////

            //next
            next = new Button();
            next.Location = new Point(845, 587);
            next.Size = new Size(210, 66);
            next.BackColor = Color.FromArgb(61, 155, 153);
            next.Font = fl.Load();
            next.Text = "Finish";
            next.Click += new EventHandler(next_click);

            //back
            back = new Button();
            back.Location = new Point(200, 587);
            back.Size = new Size(210, 66);
            back.BackColor = Color.FromArgb(61, 155, 153);
            back.Font = fl.Load();
            back.Text = "<<<Back";
            back.Click += new EventHandler(back_click);

            //check
            check = new Button();
            check.Location = new Point(630, 587);
            check.Size = new Size(210, 66);
            check.BackColor = Color.FromArgb(61, 155, 153);
            check.Font = fl.Load();
            check.Text = "Check";
            check.Click += new EventHandler(check_click);

            //listen
            listen = new Button();
            listen.Location = new Point(415, 587);
            listen.Size = new Size(210, 66);
            listen.BackColor = Color.FromArgb(61, 155, 153);
            listen.Font = fl.Load();
            listen.Text = "listen";
            listen.Click += new EventHandler(listen_click);



            /// 1
            textBox1 = new TextBox();
            textBox1.Location = new Point(353, 182);
            textBox1.Name = "textBox1";
            textBox1.Font = new Font("", 15F);
            textBox1.Size = new Size(70, 70);
            textBox1.BorderStyle = BorderStyle.None;
            textBox1.TextAlign = HorizontalAlignment.Center;

            //2
            textBox2 = new TextBox();
            textBox2.Location = new Point(500, 182);
            textBox2.Name = "textBox2";
            textBox2.Font = new Font("", 15F);
            textBox2.Size = new Size(70, 90);
            textBox2.BorderStyle = BorderStyle.None;
            textBox2.TextAlign = HorizontalAlignment.Center;


            //3
            textBox3 = new TextBox();
            textBox3.Location = new Point(497, 215);
            textBox3.Name = "textBox3";
            textBox3.Font = new Font("", 15F);
            textBox3.Size = new Size(60, 90);
            textBox3.BorderStyle = BorderStyle.None;
            textBox3.TextAlign = HorizontalAlignment.Center;

            //4
            textBox4 = new TextBox();
            textBox4.Location = new Point(171, 243);
            textBox4.Name = "textBox4";
            textBox4.Font = new Font("", 15F);
            textBox4.Size = new Size(55, 90);
            textBox4.BorderStyle = BorderStyle.None;
            textBox4.TextAlign = HorizontalAlignment.Center;

            //5
            textBox5 = new TextBox();
            textBox5.Location = new Point(487, 274);
            textBox5.Name = "textBox5";
            textBox5.Font = new Font("", 15F);
            textBox5.Size = new Size(95, 90);
            textBox5.BorderStyle = BorderStyle.None;
            textBox5.TextAlign = HorizontalAlignment.Center;

            //6
            textBox6 = new TextBox();
            textBox6.Location = new Point(323, 306);
            textBox6.Name = "textBox6";
            textBox6.Font = new Font("", 15F);
            textBox6.Size = new Size(45, 90);
            textBox6.BorderStyle = BorderStyle.None;
            textBox6.TextAlign = HorizontalAlignment.Center;

            //7
            textBox7 = new TextBox();
            textBox7.Location = new Point(335, 335);
            textBox7.Name = "textBox7";
            textBox7.Font = new Font("", 15F);
            textBox7.Size = new Size(110, 90);
            textBox7.BorderStyle = BorderStyle.None;
            textBox7.TextAlign = HorizontalAlignment.Center;

            //8
            textBox8 = new TextBox();
            textBox8.Location = new Point(175, 365);
            textBox8.Name = "textBox8";
            textBox8.Font = new Font("", 15F);
            textBox8.Size = new Size(105, 90);
            textBox8.BorderStyle = BorderStyle.None;
            textBox8.TextAlign = HorizontalAlignment.Center;

            //9
            textBox9 = new TextBox();
            textBox9.Location = new Point(808, 182);
            textBox9.Name = "textBox9";
            textBox9.Font = new Font("", 15F);
            textBox9.Size = new Size(45, 90);
            textBox9.BorderStyle = BorderStyle.None;
            textBox9.TextAlign = HorizontalAlignment.Center;

            //10
            textBox10 = new TextBox();
            textBox10.Location = new Point(877, 182);
            textBox10.Name = "textBox10";
            textBox10.Font = new Font("", 15F);
            textBox10.Size = new Size(65, 90);
            textBox10.BorderStyle = BorderStyle.None;
            textBox10.TextAlign = HorizontalAlignment.Center;

            //11
            textBox11 = new TextBox();
            textBox11.Location = new Point(847, 215);
            textBox11.Name = "textBox11";
            textBox11.Font = new Font("", 15F);
            textBox11.Size = new Size(130, 90);
            textBox11.BorderStyle = BorderStyle.None;
            textBox11.TextAlign = HorizontalAlignment.Center;

            //12
            textBox12 = new TextBox();
            textBox12.Location = new Point(977, 243);
            textBox12.Name = "textBox12";
            textBox12.Font = new Font("", 15F);
            textBox12.Size = new Size(15, 90);
            textBox12.BorderStyle = BorderStyle.None;
            textBox12.TextAlign = HorizontalAlignment.Center;

            //13
            textBox13 = new TextBox();
            textBox13.Location = new Point(1028, 243);
            textBox13.Name = "textBox13";
            textBox13.Font = new Font("", 15F);
            textBox13.Size = new Size(110, 90);
            textBox13.BorderStyle = BorderStyle.None;
            textBox13.TextAlign = HorizontalAlignment.Center;

            //14
            textBox14 = new TextBox();
            textBox14.Location = new Point(1006, 274);
            textBox14.Name = "textBox14";
            textBox14.Font = new Font("", 15F);
            textBox14.Size = new Size(70, 90);
            textBox14.BorderStyle = BorderStyle.None;
            textBox14.TextAlign = HorizontalAlignment.Center;


            //15
            textBox15 = new TextBox();
            textBox15.Location = new Point(756, 365);
            textBox15.Name = "textBox15";
            textBox15.Font = new Font("", 15F);
            textBox15.Size = new Size(125, 90);
            textBox15.BorderStyle = BorderStyle.None;
            textBox15.TextAlign = HorizontalAlignment.Center;


            //16
            textBox16 = new TextBox();
            textBox16.Location = new Point(913, 365);
            textBox16.Font = new Font("", 15F);
            textBox16.Size = new Size(52, 90);
            textBox16.BorderStyle = BorderStyle.None;
            textBox16.TextAlign = HorizontalAlignment.Center;





            mw.ResumeLayout();

            FillAnswerTable();
        }

        void FillAnswerTable()
        {
            if (answers.Count != 0)
            {
                answers.Clear();
            }

            answers.Add(new Answer(textBox1, "lane"));
            answers.Add(new Answer(textBox2, "erry"));
            answers.Add(new Answer(textBox3, "anding"));
            answers.Add(new Answer(textBox4, "ard"));
            answers.Add(new Answer(textBox5, "light"));
            answers.Add(new Answer(textBox6, "isa"));
            answers.Add(new Answer(textBox7, "uggage"));
            answers.Add(new Answer(textBox8, "rolley"));
            answers.Add(new Answer(textBox9, "ast"));
            answers.Add(new Answer(textBox10, "rain"));
            answers.Add(new Answer(textBox11, "assport"));
            answers.Add(new Answer(textBox12, "u"));
            answers.Add(new Answer(textBox13, "embers"));
            answers.Add(new Answer(textBox14, "elay"));
            answers.Add(new Answer(textBox15, "rrivals"));
            answers.Add(new Answer(textBox16, "all"));


        }

        public void AddElements()
        {

            mw.Controls.Add(textBox1);
            mw.Controls.Add(textBox2);
            mw.Controls.Add(textBox3);
            mw.Controls.Add(textBox4);
            mw.Controls.Add(textBox5);
            mw.Controls.Add(textBox6);
            mw.Controls.Add(textBox7);
            mw.Controls.Add(textBox8);
            mw.Controls.Add(textBox9);
            mw.Controls.Add(textBox10);
            mw.Controls.Add(textBox11);
            mw.Controls.Add(textBox12);
            mw.Controls.Add(textBox13);
            mw.Controls.Add(textBox14);
            mw.Controls.Add(textBox15);
            mw.Controls.Add(textBox16);
            mw.Controls.Add(listen);
            mw.Controls.Add(next);
            mw.Controls.Add(back);
            mw.Controls.Add(check);
            mw.Controls.Add(exercises);


        }

        public void RemoveElements()
        {
            mw.Controls.Remove(textBox1);
            mw.Controls.Remove(textBox2);
            mw.Controls.Remove(textBox3);
            mw.Controls.Remove(textBox4);
            mw.Controls.Remove(textBox5);
            mw.Controls.Remove(textBox6);
            mw.Controls.Remove(textBox7);
            mw.Controls.Remove(textBox8);
            mw.Controls.Remove(textBox9);
            mw.Controls.Remove(textBox10);
            mw.Controls.Remove(textBox11);
            mw.Controls.Remove(textBox12);
            mw.Controls.Remove(textBox13);
            mw.Controls.Remove(textBox14);
            mw.Controls.Remove(textBox15);
            mw.Controls.Remove(textBox16);
            mw.Controls.Remove(listen);
            mw.Controls.Remove(next);
            mw.Controls.Remove(back);
            mw.Controls.Remove(check);
            mw.Controls.Remove(exercises);
        }

        System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"media\bl2\bl2_write_ex3.wav");

        void listen_click(object sender, EventArgs e)
        {
            player.Play();
        }

        void back_click(object sender, EventArgs e)
        {
            bl2_write_ex2 bl2writeex2 = new bl2_write_ex2(mw);
            RemoveElements();
            bl2writeex2.AddElements();
            player.Stop();
        }



        void check_click(object sender, EventArgs e)
        {
            foreach (var a in answers)
            {
                if (a.element.Text != a.answer)
                {
                    a.element.ForeColor = Color.Red;
                }
                else
                {
                    a.element.ForeColor = Color.Green;
                }
            }
        }

        void next_click(object sender, EventArgs e)
        {
            bl2_write_exes bl2writeexes = new bl2_write_exes(mw);
            RemoveElements();
            bl2writeexes.AddElements();
            player.Stop();
        }
    }
}
