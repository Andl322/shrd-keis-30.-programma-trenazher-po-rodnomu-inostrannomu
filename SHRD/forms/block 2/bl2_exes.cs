﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SHRD
{
    class bl2_exes
    {
        Button btn1;
        Button btn2;
        Button btn3;
        Button btn4;
        Button btn5;
        Button back;
        PictureBox exercises;

        Form1 mw;
        FontLoader fl = new FontLoader();

        public bl2_exes(Form1 form)
        {
            ////////////загрузка основного


            mw = form;

            mw.SuspendLayout();
            /////////////////////////////


            //Фоновая картинка
            exercises = new PictureBox();
            exercises.Image = global::SHRD.Properties.Resources.exercises_2;
            exercises.Location = new Point(0, 0);
            exercises.Name = "exercises";
            exercises.TabIndex = 0;
            exercises.TabStop = false;
            exercises.Visible = true;
            exercises.Dock = DockStyle.Fill;
            exercises.SizeMode = PictureBoxSizeMode.StretchImage;
            //////


            //vocabulary
            btn1 = new Button();
            btn1.Location = new Point(330, 145);
            btn1.Size = new Size(605, 64);
            btn1.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn1.Font = fl.Load();
            btn1.Text = "Vocabulary";
            btn1.Click += new EventHandler(Bt_vocabulary_click);

            //speaking
            btn2 = new Button();
            btn2.Location = new Point(330, 245);
            btn2.Size = new Size(605, 64);
            btn2.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn2.Font = fl.Load();
            btn2.Text = "Speaking";
            btn2.Click += new EventHandler(this.Bt_speaking_click);

            //reading
            btn3 = new Button();
            btn3.Location = new Point(330, 345);
            btn3.Size = new Size(605, 64);
            btn3.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn3.Font = fl.Load();
            btn3.Text = "Reading";
            btn3.Click += new EventHandler(this.Bt_reading_click);

            //listening
            btn4 = new Button();
            btn4.Location = new Point(330, 345);
            btn4.Size = new Size(605, 64);
            btn4.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn4.Font = fl.Load();
            btn4.Text = "Listening";
            btn4.Click += new EventHandler(this.Bt_listening_click);

            //writing
            btn5 = new Button();
            btn5.Location = new Point(330, 445);
            btn5.Size = new Size(605, 64);
            btn5.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn5.Font = fl.Load();
            btn5.Text = "Writing";
            btn5.Click += new EventHandler(this.Bt_writing_click);

            //back
            back = new Button();
            back.Location = new Point(47, 590);
            back.Size = new Size(210, 66);
            back.BackColor = System.Drawing.Color.FromArgb(61, 155, 153);
            back.Font = fl.Load();
            back.Text = "<<<Back";
            back.Click += new EventHandler(back_Click);

            

            //это тоже основа
            mw.ResumeLayout();
            /////
        }

        private void Bt_speaking_click(object sender, EventArgs e)
        {
            bl2_speak_exes bl2exe = new bl2_speak_exes(mw);
            RemoveElements();
            bl2exe.AddElements();

        }

        private void Bt_reading_click(object sender, EventArgs e)
        {
            bl1_read_exes bl1readexe = new bl1_read_exes(mw);
            RemoveElements();
            bl1readexe.AddElements();

        }

        private void Bt_listening_click(object sender, EventArgs e)
        {
            bl2_listen_exes bl2listenexes = new bl2_listen_exes(mw);
            RemoveElements();
            bl2listenexes.AddElements();
        }


        //первая кнопка, vocabulary
        private void Bt_vocabulary_click(object sender, EventArgs e)
        {
            bl2_voc_exes bl2vocexes = new bl2_voc_exes(mw);
            RemoveElements();
            bl2vocexes.AddElements();

        }

        private void Bt_writing_click(object sender, EventArgs e)
        {
            bl2_write_exes bl2writeexes = new bl2_write_exes(mw);
            RemoveElements();
            bl2writeexes.AddElements();
        }



        public void AddElements()
        {
            mw.Controls.Add(btn1);
            mw.Controls.Add(btn2);
          //  mw.Controls.Add(btn3);
            mw.Controls.Add(btn4);
            mw.Controls.Add(btn5);
            mw.Controls.Add(back);
            mw.Controls.Add(exercises);
        }

        public void RemoveElements()
        {
            mw.Controls.Remove(btn1);
            mw.Controls.Remove(btn2);
           // mw.Controls.Remove(btn3);
            mw.Controls.Remove(btn4);
            mw.Controls.Remove(btn5);
            mw.Controls.Remove(back);
            mw.Controls.Remove(exercises);
        }

        void back_Click(object sender, EventArgs e)
        {
            VisitCountry vc = new VisitCountry(mw);
            RemoveElements();
            vc.AddElements();
        }
    }
}
