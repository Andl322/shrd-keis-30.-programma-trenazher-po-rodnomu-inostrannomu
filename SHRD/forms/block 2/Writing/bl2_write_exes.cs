﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SHRD
{
    class bl2_write_exes
    {
        Button btn1;
        Button btn2;
        Button btn3;
        Button back;
        PictureBox exercises;

        //Обязательно грузим шрифт

        Form1 mw;
        FontLoader fl = new FontLoader();

        ///////////


        public bl2_write_exes(Form1 form)
        {
            ////////////загрузка основного


            mw = form;

            mw.SuspendLayout();
            /////////////////////////////


            //Фоновая картинка
            exercises = new PictureBox();
            exercises.Image = global::SHRD.Properties.Resources.exercises_2;
            exercises.Location = new Point(0, 0);
            exercises.Name = "exercises";
            exercises.TabIndex = 0;
            exercises.TabStop = false;
            exercises.Visible = true;
            exercises.Dock = DockStyle.Fill;
            exercises.SizeMode = PictureBoxSizeMode.StretchImage;
            //////

            /////////exercises buttons////////////
            btn1 = new Button();
            btn1.Location = new Point(327, 175);
            btn1.Size = new Size(605, 64);
            btn1.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn1.Font = fl.Load();
            btn1.Text = "Exercise 1";
            btn1.Click += new EventHandler(btn1_click);

            btn2 = new Button();
            btn2.Location = new Point(327, 304);
            btn2.Size = new Size(605, 64);
            btn2.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn2.Font = fl.Load();
            btn2.Text = "Exercise 2";
            btn2.Click += new EventHandler(btn2_click);

            btn3 = new Button();
            btn3.Location = new Point(327, 430);
            btn3.Size = new Size(605, 64);
            btn3.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn3.Font = fl.Load();
            btn3.Text = "Exercise 3";
            btn3.Click += new EventHandler(btn3_click);
            //////////////////////////////////////////////

            //back
            back = new Button();
            back.Location = new Point(47, 590);
            back.Size = new Size(210, 66);
            back.BackColor = System.Drawing.Color.FromArgb(61, 155, 153);
            back.Font = fl.Load();
            back.Text = "<<<Back";
            back.Click += new EventHandler(back_Click);




            //это тоже основа
            mw.ResumeLayout();
            /////



        }

        public void AddElements()
        {
            mw.Controls.Add(btn1);
            mw.Controls.Add(btn2);
            //mw.Controls.Add(btn3);
            mw.Controls.Add(back);
            mw.Controls.Add(exercises);

        }

        public void RemoveElements()
        {
            mw.Controls.Remove(btn1);
            mw.Controls.Remove(btn2);
            // mw.Controls.Remove(btn3);
            mw.Controls.Remove(back);
            mw.Controls.Remove(exercises);
        }

        void btn1_click(object sender, EventArgs e)
        {
            bl2_write_ex1 bl2writeex1 = new bl2_write_ex1(mw);
            RemoveElements();
            bl2writeex1.AddElements();
        }


        void btn2_click(object sender, EventArgs e)
        {
            bl2_write_ex2 bl2writeex2 = new bl2_write_ex2(mw);
            RemoveElements();
            bl2writeex2.AddElements();
        }

        void btn3_click(object sender, EventArgs e)
        {

        }

        void back_Click(object sender, EventArgs e)
        {
            bl2_exes bl2 = new bl2_exes(mw);
            RemoveElements();
            bl2.AddElements();
        }

    }

}

