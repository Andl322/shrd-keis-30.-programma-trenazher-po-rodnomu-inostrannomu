﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SHRD
{
    class bl2_write_ex1
    {
        struct Answer
        {
            public TextBox element;
            public string answer;

            public Answer(TextBox tb, string text)
            {
                this.element = tb;
                this.answer = text;
            }
        }

        Button next;
        Button check;
        Button menu;
        Button listen;
        TextBox textBox1;
        TextBox textBox2;
        TextBox textBox3;
        TextBox textBox4;
        PictureBox exercises;

        Form1 mw;
        FontLoader fl = new FontLoader();
        List<Answer> answers = new List<Answer>();

        public bl2_write_ex1(Form1 form)
        {
            ////////////загрузка основного


            mw = form;

            mw.SuspendLayout();
            /////////////////////////////


            //Фоновая картинка
            exercises = new PictureBox();
            exercises.Image = global::SHRD.Properties.Resources.writing_bl2_1;
            exercises.Location = new Point(0, 0);
            exercises.Name = "exercises";
            exercises.TabIndex = 0;
            exercises.TabStop = false;
            exercises.Visible = true;
            exercises.Dock = DockStyle.Fill;
            exercises.SizeMode = PictureBoxSizeMode.StretchImage;
            //////

            //menu
            menu = new Button();
            menu.Location = new Point(415, 587);
            menu.Size = new Size(210, 66);
            menu.BackColor = Color.FromArgb(61, 155, 153);
            menu.Font = fl.Load();
            menu.Text = "Menu";
            menu.Click += new EventHandler(menu_click);

            //next
            next = new Button();
            next.Location = new Point(630, 587);
            next.Size = new Size(210, 66);
            next.BackColor = Color.FromArgb(61, 155, 153);
            next.Font = fl.Load();
            next.Text = "Next>>>";
            next.Click += new EventHandler(next_click);




            mw.ResumeLayout();

        }



        public void AddElements()
        {

            mw.Controls.Add(listen);
            mw.Controls.Add(menu);
            mw.Controls.Add(next);
            mw.Controls.Add(check);
            mw.Controls.Add(exercises);


        }

        public void RemoveElements()
        {
            mw.Controls.Remove(listen);
            mw.Controls.Remove(menu);
            mw.Controls.Remove(next);
            mw.Controls.Remove(check);
            mw.Controls.Remove(exercises);
        }

        void next_click(object sender, EventArgs e)
        {
            bl2_write_ex2 bl2writeex2 = new bl2_write_ex2(mw);
            RemoveElements();
            bl2writeex2.AddElements();
        }





        void menu_click(object sender, EventArgs e)
        {
            bl2_write_exes bl2writeexes = new bl2_write_exes(mw);
            RemoveElements();
            bl2writeexes.AddElements();
        }
    }
}
