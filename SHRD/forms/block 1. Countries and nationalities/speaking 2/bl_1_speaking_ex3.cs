﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SHRD
{
    class bl_1_speaking_ex3
    {
        struct Answer
        {
            public TextBox element;
            public string answer;

            public Answer(TextBox tb, string text)
            {
                this.element = tb;
                this.answer = text;
            }
        }

        Button back;
        Button check;
        Button menu;
        Button listen;
        TextBox textBox1;
        TextBox textBox2;
        PictureBox exercises;

        Form1 mw;
        FontLoader fl = new FontLoader();
        List<Answer> answers = new List<Answer>();

        public bl_1_speaking_ex3(Form1 form)
        {
            ////////////загрузка основного


            mw = form;

            mw.SuspendLayout();
            /////////////////////////////


            //Фоновая картинка
            exercises = new PictureBox();
            exercises.Image = global::SHRD.Properties.Resources.bl2_speaking_ex2;
            exercises.Location = new Point(0, 0);
            exercises.Name = "exercises";
            exercises.TabIndex = 0;
            exercises.TabStop = false;
            exercises.Visible = true;
            exercises.Dock = DockStyle.Fill;
            exercises.SizeMode = PictureBoxSizeMode.StretchImage;
            //////

            //menu
            menu = new Button();
            menu.Location = new Point(845, 587);
            menu.Size = new Size(210, 66);
            menu.BackColor = Color.FromArgb(61, 155, 153);
            menu.Font = fl.Load();
            menu.Text = "Next>>>";
            menu.Click += new EventHandler(menu_click);

            //back
            back = new Button();
            back.Location = new Point(200, 587);
            back.Size = new Size(210, 66);
            back.BackColor = Color.FromArgb(61, 155, 153);
            back.Font = fl.Load();
            back.Text = "<<<Back";
            back.Click += new EventHandler(back_click);

            //check
            check = new Button();
            check.Location = new Point(630, 587);
            check.Size = new Size(210, 66);
            check.BackColor = Color.FromArgb(61, 155, 153);
            check.Font = fl.Load();
            check.Text = "Check";
            check.Click += new EventHandler(check_click);

            //listen
            listen = new Button();
            listen.Location = new Point(415, 587);
            listen.Size = new Size(210, 66);
            listen.BackColor = Color.FromArgb(61, 155, 153);
            listen.Font = fl.Load();
            listen.Text = "listen";
            listen.Click += new EventHandler(listen_click);

            /// 1
            textBox1 = new TextBox();
            textBox1.Location = new Point(535, 452);
            textBox1.Name = "textBox1";
            textBox1.Font = new Font("", 15F);
            textBox1.Size = new Size(460, 70);
            textBox1.BorderStyle = BorderStyle.None;
            textBox1.TextAlign = HorizontalAlignment.Center;

            //2
            textBox2 = new TextBox();
            textBox2.Location = new Point(480, 485);
            textBox2.Name = "textBox2";
            textBox2.Font = new Font("", 15F);
            textBox2.Size = new Size(460, 90);
            textBox2.BorderStyle = BorderStyle.None;
            textBox2.TextAlign = HorizontalAlignment.Center;

            //this.MouseMove += new MouseEventHandler(mouseEvent);




            mw.ResumeLayout();

            FillAnswerTable();
        }

        void FillAnswerTable()
        {
            if (answers.Count != 0)
            {
                answers.Clear();
            }

            answers.Add(new Answer(textBox1, "name's Anna Nova, I'm from Prague"));
            answers.Add(new Answer(textBox2, "Czech, I'm a teacher"));


        }

        ////////////////ПЕРЕМЕЩЕНИЕ
        private void mouseEvent(object sender, MouseEventArgs e)
        {
            listen.Location = new Point(Cursor.Position.X, Cursor.Position.Y);
        }

        public void AddElements()
        {

            mw.Controls.Add(textBox1);
            mw.Controls.Add(textBox2);
            mw.Controls.Add(listen);
            mw.Controls.Add(menu);
            mw.Controls.Add(back);
            mw.Controls.Add(check);
            mw.Controls.Add(exercises);


        }

        public void RemoveElements()
        {
            mw.Controls.Remove(textBox1);
            mw.Controls.Remove(textBox2);
            mw.Controls.Remove(listen);
            mw.Controls.Remove(menu);
            mw.Controls.Remove(back);
            mw.Controls.Remove(check);
            mw.Controls.Remove(exercises);
        }

        void back_click(object sender, EventArgs e)
        {
            bl_1_speaking_ex2 bl2speakexes = new bl_1_speaking_ex2(mw);
            RemoveElements();
            bl2speakexes.AddElements();
            player.Stop();
        }

        System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"media\bl2\bl2_speak_ex2.wav");

        void listen_click(object sender, EventArgs e)
        {
            //поменять звук на правильный


                player.Play();
            
        }

        void check_click(object sender, EventArgs e)
        {
            foreach (var a in answers)
            {
                if (a.element.Text != a.answer)
                {
                    a.element.ForeColor = Color.Red;
                }
                else
                {
                    a.element.ForeColor = Color.Green;
                }
            }
        }

        void menu_click(object sender, EventArgs e)
        {
            bl_1_speak_ex4_p1 bl2spex3 = new bl_1_speak_ex4_p1(mw);
            RemoveElements();
            bl2spex3.AddElements();
            player.Stop();
        }
    }
}
