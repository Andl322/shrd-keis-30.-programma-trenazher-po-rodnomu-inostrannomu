﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;


namespace SHRD
{
    class bl_1_speaking_ex2
    {
        struct Answer
        {
            public TextBox element;
            public string answer;

            public Answer(TextBox tb, string text)
            {
                this.element = tb;
                this.answer = text;
            }
        }

        Button next;
        Button check;
        Button back;
        Button menu;
        TextBox textBox1;
        TextBox textBox2;
        TextBox textBox3;
        TextBox textBox4;
        TextBox textBox5;
        TextBox textBox6;
        TextBox textBox7;
        TextBox textBox8;
        TextBox textBox9;
        TextBox textBox10;

        PictureBox speaking2;



        Form1 mw;
        FontLoader fl = new FontLoader();

        List<Answer> answers = new List<Answer>();

        public bl_1_speaking_ex2(Form1 form)
        {
            ///////////загрузка основного

            mw = form;

            mw.SuspendLayout();

            /////////////////////////////



            //Фоновая картинка
            speaking2 = new PictureBox();
            speaking2.Image = global::SHRD.Properties.Resources.speaking53;
            speaking2.Location = new Point(0, 0);
            speaking2.Name = "speaking1";
            speaking2.TabIndex = 0;
            speaking2.TabStop = false;
            speaking2.Visible = true;
            speaking2.Dock = DockStyle.Fill;
            speaking2.SizeMode = PictureBoxSizeMode.StretchImage;
            //////


            

            //check
            check = new Button();
            check.Location = new Point(510, 587);
            check.Size = new Size(210, 66);
            check.BackColor = Color.FromArgb(61, 155, 153);
            check.Font = fl.Load();
            check.Text = "Check";
            check.Click += new EventHandler(check_click);

            //menu
            menu = new Button();
            menu.Location = new Point(725, 587);
            menu.Size = new Size(210, 66);
            menu.BackColor = Color.FromArgb(61, 155, 153);
            menu.Font = fl.Load();
            menu.Text = "Next>>>";
            menu.Click += new EventHandler(menu_click);

            //back
            back = new Button();
            back.Location = new Point(295, 587);
            back.Size = new Size(210, 66);
            back.BackColor = Color.FromArgb(61, 155, 153);
            back.Font = fl.Load();
            back.Text = "<<<Back";
            back.Click += new EventHandler(back_click);


            /// 1
            textBox1 = new TextBox();
            textBox1.Location = new Point(778, 135);
            textBox1.Name = "textBox1";
            textBox1.Font = new Font("", 17F);
            textBox1.Size = new Size(300, 70);
            textBox1.BorderStyle = BorderStyle.None;
            textBox1.TextAlign = HorizontalAlignment.Center;

            //2
            textBox2 = new TextBox();
            textBox2.Location = new Point(778, 184);
            textBox2.Name = "textBox2";
            textBox2.Font = new Font("", 15F);
            textBox2.Size = new Size(250, 90);
            textBox2.BorderStyle = BorderStyle.None;
            textBox2.TextAlign = HorizontalAlignment.Center;


            //3
            textBox3 = new TextBox();
            textBox3.Location = new Point(790, 228);
            textBox3.Name = "textBox3";
            textBox3.Font = new Font("", 15F);
            textBox3.Size = new Size(315, 90);
            textBox3.BorderStyle = BorderStyle.None;
            textBox3.TextAlign = HorizontalAlignment.Center;


            //4
            textBox4 = new TextBox();
            textBox4.Location = new Point(780, 274);
            textBox4.Name = "textBox4";
            textBox4.Font = new Font("", 15F);
            textBox4.Size = new Size(77, 100);
            textBox4.BorderStyle = BorderStyle.None;
            textBox4.TextAlign = HorizontalAlignment.Center;


            //5
            textBox5 = new TextBox();
            textBox5.Location = new Point(920, 274);
            textBox5.Name = "textBox5";
            textBox5.Font = new Font("", 15F);
            textBox5.Size = new Size(100, 90);
            textBox5.BorderStyle = BorderStyle.None;
            textBox5.TextAlign = HorizontalAlignment.Center;

            //6
            textBox6 = new TextBox();
            textBox6.Location = new Point(737, 320);
            textBox6.Name = "textBox6";
            textBox6.Font = new Font("", 15F);
            textBox6.Size = new Size(108, 90);
            textBox6.BorderStyle = BorderStyle.None;
            textBox6.TextAlign = HorizontalAlignment.Center;

            //7
            textBox7 = new TextBox();
            textBox7.Location = new Point(862, 320);
            textBox7.Name = "textBox7";
            textBox7.Font = new Font("", 15F);
            textBox7.Size = new Size(90, 90);
            textBox7.BorderStyle = BorderStyle.None;
            textBox7.TextAlign = HorizontalAlignment.Center;


            //8
            textBox8 = new TextBox();
            textBox8.Location = new Point(1023, 320);
            textBox8.Name = "textBox8";
            textBox8.Font = new Font("", 15F);
            textBox8.Size = new Size(75, 90);
            textBox8.BorderStyle = BorderStyle.None;
            textBox8.TextAlign = HorizontalAlignment.Center;

            //9
            textBox9 = new TextBox();
            textBox9.Location = new Point(780, 364);
            textBox9.Name = "textBox9";
            textBox9.Font = new Font("", 15F);
            textBox9.Size = new Size(325, 90);
            textBox9.BorderStyle = BorderStyle.None;
            textBox9.TextAlign = HorizontalAlignment.Center;

            //10
            textBox10 = new TextBox();
            textBox10.Location = new Point(737, 410);
            textBox10.Name = "textBox10";
            textBox10.Font = new Font("", 15F);
            textBox10.Size = new Size(260, 90);
            textBox10.BorderStyle = BorderStyle.None;
            textBox10.TextAlign = HorizontalAlignment.Center;



            mw.ResumeLayout();

            FillAnswerTable();

        }

        void FillAnswerTable()
        {
            if (answers.Count != 0)
            {
                answers.Clear();
            }

            answers.Add(new Answer(textBox1, "I am"));
            answers.Add(new Answer(textBox2, "name's"));
            answers.Add(new Answer(textBox3, "to meet you"));
            answers.Add(new Answer(textBox4, "she isn't"));
            answers.Add(new Answer(textBox5, "is from"));
            answers.Add(new Answer(textBox6, "fine"));
            answers.Add(new Answer(textBox7, "thanks"));
            answers.Add(new Answer(textBox8, "you"));
            answers.Add(new Answer(textBox9, "I am not"));
            answers.Add(new Answer(textBox10, "am from"));

            
        }

        public void AddElements()
        {
            mw.Controls.Add(textBox1);
            mw.Controls.Add(textBox2);
            mw.Controls.Add(textBox3);
            mw.Controls.Add(textBox4);
            mw.Controls.Add(textBox5);
            mw.Controls.Add(textBox6);
            mw.Controls.Add(textBox7);
            mw.Controls.Add(textBox8);
            mw.Controls.Add(textBox9);
            mw.Controls.Add(textBox10);
            mw.Controls.Add(next);
            mw.Controls.Add(check);
            mw.Controls.Add(back);
            mw.Controls.Add(menu);
            mw.Controls.Add(speaking2);
         

        }

        public void RemoveElements()
        {
            mw.Controls.Remove(textBox1);
            mw.Controls.Remove(textBox2);
            mw.Controls.Remove(textBox3);
            mw.Controls.Remove(textBox4);
            mw.Controls.Remove(textBox5);
            mw.Controls.Remove(textBox6);
            mw.Controls.Remove(textBox7);
            mw.Controls.Remove(textBox8);
            mw.Controls.Remove(textBox9);
            mw.Controls.Remove(textBox10);
            mw.Controls.Remove(next);
            mw.Controls.Remove(check);
            mw.Controls.Remove(back);
            mw.Controls.Remove(menu);
            mw.Controls.Remove(speaking2);
        }

        void next_click(object sender, EventArgs e)
        {

        }

        void check_click(object sender, EventArgs e)
        {
            foreach (var a in answers)
            {
                if (a.element.Text != a.answer)
                {
                    a.element.ForeColor = Color.Red;
                }
                else
                {
                    a.element.ForeColor = Color.Black;
                }
            }
        }
        void menu_click(object sender, EventArgs e)
        {
            bl_1_speaking_ex3 bl1sp = new bl_1_speaking_ex3(mw);
            RemoveElements();
            bl1sp.AddElements();
        }


        void back_click(object sender, EventArgs e)
        {
            bl1_speaking_ex1 bl1ex1 = new bl1_speaking_ex1(mw);
            RemoveElements();
            bl1ex1.AddElements();
        }
    }
}
