﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SHRD
{
    class bl1_speaking_ex1
    {

        //проверка ответов
        struct Answer
        {
            public TextBox element;
            public string answer;

            public Answer(TextBox tb, string text)
            {
                this.element = tb;
                this.answer = text;
            }
        }
        
        ////////////

        Button next;
        Button check;
        Button back;
        Button listen;
        PictureBox speaking1;
        TextBox textBox1;
        TextBox textBox2;
        TextBox textBox3;
        TextBox textBox4;
        TextBox textBox5;
        TextBox textBox6;
        TextBox textBox7;
        TextBox textBox8;

        TextBox SueTextbox1;
        TextBox SueTextbox2;
        TextBox SueTextbox3;
        TextBox SueTextbox4;
        TextBox SueTextbox5;
        TextBox SueTextbox6;

        Form1 mw;
        FontLoader fl = new FontLoader();

        List<Answer> answers = new List<Answer>();
        
        public bl1_speaking_ex1(Form1 form)
        {
            ///////////загрузка основного

            mw = form;

            mw.SuspendLayout();

            /////////////////////////////


            //Фоновая картинка
            speaking1 = new PictureBox();
            speaking1.Image = global::SHRD.Properties.Resources.speaking21;
            speaking1.Location = new Point(0, 0);
            speaking1.Name = "speaking1";
            speaking1.TabIndex = 0;
            speaking1.TabStop = false;
            speaking1.Visible = true;
            speaking1.Dock = DockStyle.Fill;
            speaking1.SizeMode = PictureBoxSizeMode.StretchImage;
            //////



            //next
            next = new Button();
            next.Location = new Point(850, 587);
            next.Size = new Size(210, 66);
            next.BackColor = System.Drawing.Color.FromArgb(61, 155, 153);
            next.Font = fl.Load();
            next.Text = "next>>>";
            next.Click += new EventHandler(next_click);

            //check
            check = new Button();
            check.Location = new Point(635, 587);
            check.Size = new Size(210, 66);
            check.BackColor = System.Drawing.Color.FromArgb(61, 155, 153);
            check.Font = fl.Load();
            check.Text = "Check";
            check.Click += new EventHandler(check_click);

            
            //listen
            listen = new Button();
            listen.Location = new Point(420, 587);
            listen.Size = new Size(210, 66);
            listen.BackColor = Color.FromArgb(61, 155, 153);
            listen.Font = fl.Load();
            listen.Text = "listen";
            listen.Click += new EventHandler(listen_click);

            //back
            back = new Button();
            back.Location = new Point(205, 587);
            back.Size = new Size(210, 66);
            back.BackColor = System.Drawing.Color.FromArgb(61, 155, 153);
            back.Font = fl.Load();
            back.Text = "Menu";
            back.Click += new EventHandler(back_click);




            //////kioko's text boxes//////
            ///names 1
            textBox1 = new TextBox();
            textBox1.Location = new Point(156, 374);
            textBox1.Name = "textBox1";
            textBox1.Font = new Font("", 15F);
            textBox1.Size = new Size(94, 90);
            textBox1.BorderStyle = BorderStyle.None;
            textBox1.TextAlign = HorizontalAlignment.Center;

            //japan 2
            textBox2 = new TextBox();
            textBox2.Location = new Point(498, 374);
            textBox2.Name = "textBox2";
            textBox2.Font = new Font("", 15F);
            textBox2.Size = new Size(94, 90);
            textBox2.BorderStyle = BorderStyle.None;
            textBox2.TextAlign = HorizontalAlignment.Center;


            //nice 3
            textBox3 = new TextBox();
            textBox3.Location = new Point(42, 404);
            textBox3.Name = "textBox3";
            textBox3.Font = new Font("", 15F);
            textBox3.Size = new Size(94, 90);
            textBox3.BorderStyle = BorderStyle.None;
            textBox3.TextAlign = HorizontalAlignment.Center;


            //you 4
            textBox4 = new TextBox();
            textBox4.Location = new Point(240, 404);
            textBox4.Name = "textBox4";
            textBox4.Font = new Font("", 15F);
            textBox4.Size = new Size(64, 90);
            textBox4.BorderStyle = BorderStyle.None;
            textBox4.TextAlign = HorizontalAlignment.Center;


            //im 5
            textBox5 = new TextBox();
            textBox5.Location = new Point(395, 404);
            textBox5.Name = "textBox5";
            textBox5.Font = new Font("", 15F);
            textBox5.Size = new Size(94, 90);
            textBox5.BorderStyle = BorderStyle.None;
            textBox5.TextAlign = HorizontalAlignment.Center;

            //from 6
            textBox6 = new TextBox();
            textBox6.Location = new Point(185, 434);
            textBox6.Name = "textBox6";
            textBox6.Font = new Font("", 15F);
            textBox6.Size = new Size(124, 90);
            textBox6.BorderStyle = BorderStyle.None;
            textBox6.TextAlign = HorizontalAlignment.Center;

            //this 7
            textBox7 = new TextBox();
            textBox7.Location = new Point(415, 434);
            textBox7.Name = "textBox7";
            textBox7.Font = new Font("", 15F);
            textBox7.Size = new Size(100, 90);
            textBox7.BorderStyle = BorderStyle.None;
            textBox7.TextAlign = HorizontalAlignment.Center;


            //my 8
            textBox8 = new TextBox();
            textBox8.Location = new Point(535, 434);
            textBox8.Name = "textBox8";
            textBox8.Font = new Font("", 15F);
            textBox8.Size = new Size(90, 90);
            textBox8.BorderStyle = BorderStyle.None;
            textBox8.TextAlign = HorizontalAlignment.Center;



            //Второй диалог//////

            /// 1
            SueTextbox1 = new TextBox();
            SueTextbox1.Location = new Point(885, 374);
            SueTextbox1.Name = "SueTextbox1";
            SueTextbox1.Font = new Font("", 15F);
            SueTextbox1.Size = new Size(75, 90);
            SueTextbox1.BorderStyle = BorderStyle.None;
            SueTextbox1.TextAlign = HorizontalAlignment.Center;

            ///2
            SueTextbox2 = new TextBox();
            SueTextbox2.Location = new Point(970, 374);
            SueTextbox2.Name = "SueTextbox2";
            SueTextbox2.Font = new Font("", 15F);
            SueTextbox2.Size = new Size(75, 90);
            SueTextbox2.BorderStyle = BorderStyle.None;
            SueTextbox2.TextAlign = HorizontalAlignment.Center;

            ///3
            SueTextbox3 = new TextBox();
            SueTextbox3.Location = new Point(730, 404);
            SueTextbox3.Name = "SueTextbox3";
            SueTextbox3.Font = new Font("", 15F);
            SueTextbox3.Size = new Size(75, 90);
            SueTextbox3.BorderStyle = BorderStyle.None;
            SueTextbox3.TextAlign = HorizontalAlignment.Center;

            ///4
            SueTextbox4 = new TextBox();
            SueTextbox4.Location = new Point(815, 404);
            SueTextbox4.Name = "SueTextbox4";
            SueTextbox4.Font = new Font("", 15F);
            SueTextbox4.Size = new Size(75, 90);
            SueTextbox4.BorderStyle = BorderStyle.None;
            SueTextbox4.TextAlign = HorizontalAlignment.Center;

            ///5
            SueTextbox5 = new TextBox();
            SueTextbox5.Location = new Point(1035, 404);
            SueTextbox5.Name = "SueTextbox5";
            SueTextbox5.Font = new Font("", 15F);
            SueTextbox5.Size = new Size(90, 90);
            SueTextbox5.BorderStyle = BorderStyle.None;
            SueTextbox5.TextAlign = HorizontalAlignment.Center;

            ///6
            SueTextbox6 = new TextBox();
            SueTextbox6.Location = new Point(795, 434);
            SueTextbox6.Name = "SueTextbox6";
            SueTextbox6.Font = new Font("", 15F);
            SueTextbox6.Size = new Size(100, 90);
            SueTextbox6.BorderStyle = BorderStyle.None;
            SueTextbox6.TextAlign = HorizontalAlignment.Center;

            mw.ResumeLayout();

            FillAnswerTable();
        }

        void FillAnswerTable ()
        {
            if (answers.Count != 0)
            {
                answers.Clear();
            }

            answers.Add(new Answer(textBox1, "name's"));
            answers.Add(new Answer(textBox2, "Japan"));
            answers.Add(new Answer(textBox3, "Nice"));
            answers.Add(new Answer(textBox4, "you"));
            answers.Add(new Answer(textBox5, "I'm"));
            answers.Add(new Answer(textBox6, "from"));
            answers.Add(new Answer(textBox7, "this"));
            answers.Add(new Answer(textBox8, "my"));

            answers.Add(new Answer(SueTextbox1, "are"));
            answers.Add(new Answer(SueTextbox2, "you"));
            answers.Add(new Answer(SueTextbox3, "I'm"));
            answers.Add(new Answer(SueTextbox4, "fine"));
            answers.Add(new Answer(SueTextbox5, "you"));
            answers.Add(new Answer(SueTextbox6, "thanks"));
        }

        public void AddElements()
        {

            mw.Controls.Add(SueTextbox6);
            mw.Controls.Add(SueTextbox5);
            mw.Controls.Add(SueTextbox4);
            mw.Controls.Add(SueTextbox3);
            mw.Controls.Add(SueTextbox2);
            mw.Controls.Add(SueTextbox1);
            mw.Controls.Add(textBox8);
            mw.Controls.Add(textBox7);
            mw.Controls.Add(textBox6);
            mw.Controls.Add(textBox5);
            mw.Controls.Add(textBox4);
            mw.Controls.Add(textBox3);
            mw.Controls.Add(textBox2);
            mw.Controls.Add(textBox1);
            mw.Controls.Add(next);
            mw.Controls.Add(check);
            mw.Controls.Add(back);
            mw.Controls.Add(listen);
            mw.Controls.Add(speaking1);

        }

        public void RemoveElements()
        {
            mw.Controls.Remove(SueTextbox6);
            mw.Controls.Remove(SueTextbox5);
            mw.Controls.Remove(SueTextbox4);
            mw.Controls.Remove(SueTextbox3);
            mw.Controls.Remove(SueTextbox2);
            mw.Controls.Remove(SueTextbox1);
            mw.Controls.Remove(textBox8);
            mw.Controls.Remove(textBox7);
            mw.Controls.Remove(textBox6);
            mw.Controls.Remove(textBox5);
            mw.Controls.Remove(textBox4);
            mw.Controls.Remove(textBox3);
            mw.Controls.Remove(textBox2);
            mw.Controls.Remove(textBox1);
            mw.Controls.Remove(next);
            mw.Controls.Remove(check);
            mw.Controls.Remove(back);
            mw.Controls.Remove(listen);
            mw.Controls.Remove(speaking1);
        }



        void next_click(object sender, EventArgs e)
        {
            
            bl_1_speaking_ex2 bl2sp = new bl_1_speaking_ex2(mw);
            RemoveElements();
            bl2sp.AddElements();
            player.Stop();
            
            /*
            if (textBox1.Text == "name's" && textBox2.Text == "Japan" && textBox3.Text == "Nice" && textBox4.Text == "you" && textBox5.Text == "I'm" && textBox6.Text == "from" && textBox7.Text == "this" && textBox8.Text == "my" &&
                SueTextbox1.Text == "are" && SueTextbox2.Text == "you" && SueTextbox3.Text == "I'm" && SueTextbox4.Text == "fine" && SueTextbox5.Text == "you" && SueTextbox6.Text == "thanks")   
            {
                RemoveElements();
                MessageBox.Show("работает");
            }
            else
                MessageBox.Show("Не работает");
            */

           
        }

        System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"media\bl1\bl_1_speaking_ex1.wav");

        void listen_click(object sender, EventArgs e)
        {
            player.Play();
        }

        void check_click(object sender, EventArgs e)
        {
            foreach (var a in answers)
            {
                if (a.element.Text != a.answer)
                {
                    a.element.ForeColor = Color.Red;
                }
                else
                {
                    a.element.ForeColor = Color.Black;
                }
            }
        }

        void back_click(object sender, EventArgs e)
        {
            bl_1_speak_exes bl1sp = new bl_1_speak_exes(mw);
            RemoveElements();
            bl1sp.AddElements();
            player.Stop();
        }

        
    }
}
