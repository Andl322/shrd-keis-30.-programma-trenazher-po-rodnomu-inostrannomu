﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Media;

namespace SHRD
{
    class bl1_read_ex2
    {

        Button next;
        Button listen;
        Button menu;
        Button back;
        PictureBox reading1;

        Form1 mw;
        FontLoader fl = new FontLoader();

        public bl1_read_ex2(Form1 form)
        {
            ///////////загрузка основного

            mw = form;

            mw.SuspendLayout();

            /////////////////////////////

            //Фоновая картинка
            reading1 = new PictureBox();
            reading1.Image = global::SHRD.Properties.Resources.ex23reading;
            reading1.Location = new Point(0, 0);
            reading1.Name = "reading1";
            reading1.TabIndex = 0;
            reading1.TabStop = false;
            reading1.Visible = true;
            reading1.Dock = DockStyle.Fill;
            reading1.SizeMode = PictureBoxSizeMode.StretchImage;
            //////

            //next
            next = new Button();
            next.Location = new Point(845, 587);
            next.Size = new Size(210, 66);
            next.BackColor = Color.FromArgb(61, 155, 153);
            next.Font = fl.Load();
            next.Text = "Next>>>";
            next.Click += new EventHandler(next_click);

            //menu
            menu = new Button();
            menu.Location = new Point(415, 587);
            menu.Size = new Size(210, 66);
            menu.BackColor = Color.FromArgb(61, 155, 153);
            menu.Font = fl.Load();
            menu.Text = "Menu";
            menu.Click += new EventHandler(menu_click);

            //listen
            listen = new Button();
            listen.Location = new Point(630, 587);
            listen.Size = new Size(210, 66);
            listen.BackColor = Color.FromArgb(61, 155, 153);
            listen.Font = fl.Load();
            listen.Text = "listen";
            listen.Click += new EventHandler(listen_click);

            //back
            back = new Button();
            back.Location = new Point(200, 587);
            back.Size = new Size(210, 66);
            back.BackColor = Color.FromArgb(61, 155, 153);
            back.Font = fl.Load();
            back.Text = "<<<Back";
            back.Click += new EventHandler(back_click);

            mw.ResumeLayout();

        }

        public void AddElements()
        {
            mw.Controls.Add(next);
            mw.Controls.Add(listen);
            mw.Controls.Add(menu);
            mw.Controls.Add(back);
            mw.Controls.Add(reading1);
        }

        public void RemoveElements()
        {
            mw.Controls.Remove(next);
            mw.Controls.Remove(listen);
            mw.Controls.Remove(menu);
            mw.Controls.Remove(back);
            mw.Controls.Remove(reading1);
        }


        void next_click(object sender, EventArgs e)
        {
            bl1_read_ex3 bl1readex3 = new bl1_read_ex3(mw);
            RemoveElements();
            bl1readex3.AddElements();
            //bl1readex3.ShowElements();
            player.Stop();
        }

        void menu_click(object sender, EventArgs e)
        {
            bl1_read_exes bl1sp = new bl1_read_exes(mw);
            RemoveElements();
            bl1sp.AddElements();
            player.Stop();
        }
        System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"media\bl1\bl_1_reading_ex2.wav");
        void listen_click(object sender, EventArgs e)
        {
            //тут заглушка вместо диалога
            

            //прикрутить таймер надо для текста кнопки, чтобы listen на stop менялось, а то я так и не понял

            player.Play();

        }
        void back_click(object sender, EventArgs e)
        {
            bl1_read_ex1 bl1readex1 = new bl1_read_ex1(mw);
            RemoveElements();
            bl1readex1.AddElements();
            player.Stop();
        }
    }
}
