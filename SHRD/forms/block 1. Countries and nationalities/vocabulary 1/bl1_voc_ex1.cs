﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SHRD
{
    class bl1_voc_ex1
    {
        struct Answer
        {
            public TextBox element;
            public string answer;

            public Answer(TextBox tb, string text)
            {
                this.element = tb;
                this.answer = text;
            }
        }

        Button next;
        Button check;
        Button menu;
        TextBox textBox1;
        TextBox textBox2;
        TextBox textBox3;
        TextBox textBox4;
        TextBox textBox5;
        TextBox textBox6;
        PictureBox exercises;

        Form1 mw;
        FontLoader fl = new FontLoader();
        List<Answer> answers = new List<Answer>();

        public bl1_voc_ex1(Form1 form)
        {
            ////////////загрузка основного


            mw = form;

            mw.SuspendLayout();
            /////////////////////////////


            //Фоновая картинка
            exercises = new PictureBox();
            exercises.Image = global::SHRD.Properties.Resources.Flags3;
            exercises.Location = new Point(0, 0);
            exercises.Name = "exercises";
            exercises.TabIndex = 0;
            exercises.TabStop = false;
            exercises.Visible = true;
            exercises.Dock = DockStyle.Fill;
            exercises.SizeMode = PictureBoxSizeMode.StretchImage;
            //////

            //menu
            menu = new Button();
            menu.Location = new Point(295, 587);
            menu.Size = new Size(210, 66);
            menu.BackColor = Color.FromArgb(61, 155, 153);
            menu.Font = fl.Load();
            menu.Text = "Menu";
            menu.Click += new EventHandler(menu_click);

            //check
            check = new Button();
            check.Location = new Point(510, 587);
            check.Size = new Size(210, 66);
            check.BackColor = Color.FromArgb(61, 155, 153);
            check.Font = fl.Load();
            check.Text = "Check";
            check.Click += new EventHandler(check_click);

            //next
            next = new Button();
            next.Location = new Point(725, 587);
            next.Size = new Size(210, 66);
            next.BackColor = Color.FromArgb(61, 155, 153);
            next.Font = fl.Load();
            next.Text = "Next>>>";
            next.Click += new EventHandler(next_click);

            /// 1
            textBox1 = new TextBox();
            textBox1.Location = new Point(275, 180);
            textBox1.Name = "textBox1";
            textBox1.Font = new Font("", 17F);
            textBox1.Size = new Size(150, 70);
           // textBox1.BorderStyle = BorderStyle.None;
            textBox1.TextAlign = HorizontalAlignment.Center;

            //2
            textBox2 = new TextBox();
            textBox2.Location = new Point(275, 330);
            textBox2.Name = "textBox2";
            textBox2.Font = new Font("", 17F);
            textBox2.Size = new Size(150, 90);
            //textBox2.BorderStyle = BorderStyle.None;
            textBox2.TextAlign = HorizontalAlignment.Center;


            //3
            textBox3 = new TextBox();
            textBox3.Location = new Point(275, 470);
            textBox3.Name = "textBox3";
            textBox3.Font = new Font("", 17F);
            textBox3.Size = new Size(150, 90);
            //textBox3.BorderStyle = BorderStyle.None;
            textBox3.TextAlign = HorizontalAlignment.Center;


            //4
            textBox4 = new TextBox();
            textBox4.Location = new Point(880, 180);
            textBox4.Name = "textBox4";
            textBox4.Font = new Font("", 17F);
            textBox4.Size = new Size(150, 100);
            //textBox4.BorderStyle = BorderStyle.None;
            textBox4.TextAlign = HorizontalAlignment.Center;


            //5
            textBox5 = new TextBox();
            textBox5.Location = new Point(880, 330);
            textBox5.Name = "textBox5";
            textBox5.Font = new Font("", 17F);
            textBox5.Size = new Size(150, 90);
            //textBox5.BorderStyle = BorderStyle.None;
            textBox5.TextAlign = HorizontalAlignment.Center;

            //6
            textBox6 = new TextBox();
            textBox6.Location = new Point(880, 470);
            textBox6.Name = "textBox6";
            textBox6.Font = new Font("", 17F);
            textBox6.Size = new Size(150, 90);
            //textBox6.BorderStyle = BorderStyle.None;
            textBox6.TextAlign = HorizontalAlignment.Center;

            mw.ResumeLayout();

            FillAnswerTable();
        }

        void FillAnswerTable()
        {
            if (answers.Count != 0)
            {
                answers.Clear();
            }

            answers.Add(new Answer(textBox1, "Switzerland"));
            answers.Add(new Answer(textBox2, "Sweden"));
            answers.Add(new Answer(textBox3, "Great Britain"));
            answers.Add(new Answer(textBox4, "Germany"));
            answers.Add(new Answer(textBox5, "Italy"));
            answers.Add(new Answer(textBox6, "Poland"));
            
        }

        public void AddElements()
        {

            
           
            mw.Controls.Add(textBox6);
            mw.Controls.Add(textBox5);
            mw.Controls.Add(textBox4);
            mw.Controls.Add(textBox3);
            mw.Controls.Add(textBox2);
            mw.Controls.Add(textBox1);
            mw.Controls.Add(menu);
            mw.Controls.Add(next);
            mw.Controls.Add(check);
            mw.Controls.Add(exercises);
            

        }

        public void RemoveElements()
        {
           
            mw.Controls.Remove(textBox6);
            mw.Controls.Remove(textBox5);
            mw.Controls.Remove(textBox4);
            mw.Controls.Remove(textBox3);
            mw.Controls.Remove(textBox2);
            mw.Controls.Remove(textBox1);
            mw.Controls.Remove(menu);
            mw.Controls.Remove(next);
            mw.Controls.Remove(check);
            mw.Controls.Remove(exercises);
        }

        void next_click(object sender, EventArgs e)
        {
            bl1_voc_ex2 bl1vocex2 = new bl1_voc_ex2(mw);
            RemoveElements();
            bl1vocex2.AddElements();
        }

        void check_click(object sender, EventArgs e)
        {
            foreach (var a in answers)
            {
                if (a.element.Text != a.answer)
                {
                    a.element.ForeColor = Color.Red;
                }
                else
                {
                    a.element.ForeColor = Color.Green;
                }
            }
        }

        void menu_click(object sender, EventArgs e)
        {
            bl1_voc_exes bl1vocexes = new bl1_voc_exes(mw);
            RemoveElements();
            bl1vocexes.AddElements();
        }
    }
}