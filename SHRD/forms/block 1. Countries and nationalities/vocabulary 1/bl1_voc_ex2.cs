﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SHRD
{
    class bl1_voc_ex2
    {
        struct Answer
        {
            public TextBox element;
            public string answer;

            public Answer(TextBox tb, string text)
            {
                this.element = tb;
                this.answer = text;
            }
        }

        Button back;
        Button check;
        Button menu;
        TextBox textBox1;
        TextBox textBox2;
        TextBox textBox3;
        PictureBox exercises;

        Form1 mw;
        FontLoader fl = new FontLoader();
        List<Answer> answers = new List<Answer>();

        public bl1_voc_ex2(Form1 form)
        {
            ////////////загрузка основного


            mw = form;

            mw.SuspendLayout();
            /////////////////////////////


            //Фоновая картинка
            exercises = new PictureBox();
            exercises.Image = global::SHRD.Properties.Resources.nationalities;
            exercises.Location = new Point(0, 0);
            exercises.Name = "exercises";
            exercises.TabIndex = 0;
            exercises.TabStop = false;
            exercises.Visible = true;
            exercises.Dock = DockStyle.Fill;
            exercises.SizeMode = PictureBoxSizeMode.StretchImage;
            //////

            //menu
            menu = new Button();
            menu.Location = new Point( 725, 587);
            menu.Size = new Size(210, 66);
            menu.BackColor = Color.FromArgb(61, 155, 153);
            menu.Font = fl.Load();
            menu.Text = "Finish";
            menu.Click += new EventHandler(menu_click);

            //check
            check = new Button();
            check.Location = new Point(510, 587);
            check.Size = new Size(210, 66);
            check.BackColor = Color.FromArgb(61, 155, 153);
            check.Font = fl.Load();
            check.Text = "Check";
            check.Click += new EventHandler(check_click);

            //back
            back = new Button();
            back.Location = new Point(295, 587);
            back.Size = new Size(210, 66);
            back.BackColor = Color.FromArgb(61, 155, 153);
            back.Font = fl.Load();
            back.Text = "<<<Back";
            back.Click += new EventHandler(back_click);

            /// 1
            textBox1 = new TextBox();
            textBox1.Location = new Point(170, 360);
            textBox1.Name = "textBox1";
            textBox1.Font = new Font("", 17F);
            textBox1.Size = new Size(150, 70);
            // textBox1.BorderStyle = BorderStyle.None;
            textBox1.TextAlign = HorizontalAlignment.Center;

            //2
            textBox2 = new TextBox();
            textBox2.Location = new Point(540, 360);
            textBox2.Name = "textBox2";
            textBox2.Font = new Font("", 17F);
            textBox2.Size = new Size(150, 90);
            //textBox2.BorderStyle = BorderStyle.None;
            textBox2.TextAlign = HorizontalAlignment.Center;


            //3
            textBox3 = new TextBox();
            textBox3.Location = new Point(925, 360);
            textBox3.Name = "textBox3";
            textBox3.Font = new Font("", 17F);
            textBox3.Size = new Size(150, 90);
            //textBox3.BorderStyle = BorderStyle.None;
            textBox3.TextAlign = HorizontalAlignment.Center;


           

            mw.ResumeLayout();

            FillAnswerTable();
        }

        void FillAnswerTable()
        {
            if (answers.Count != 0)
            {
                answers.Clear();
            }

            answers.Add(new Answer(textBox1, "British"));
            answers.Add(new Answer(textBox2, "German"));
            answers.Add(new Answer(textBox3, "Russian"));

        }

        public void AddElements()
        {

            mw.Controls.Add(textBox3);
            mw.Controls.Add(textBox2);
            mw.Controls.Add(textBox1);
            mw.Controls.Add(menu);
            mw.Controls.Add(back);
            mw.Controls.Add(check);
            mw.Controls.Add(exercises);


        }

        public void RemoveElements()
        { 
            mw.Controls.Remove(textBox3);
            mw.Controls.Remove(textBox2);
            mw.Controls.Remove(textBox1);
            mw.Controls.Remove(menu);
            mw.Controls.Remove(back);
            mw.Controls.Remove(check);
            mw.Controls.Remove(exercises);
        }

        void back_click(object sender, EventArgs e)
        {
            bl1_voc_ex1 bl1vocex1 = new bl1_voc_ex1(mw);
            RemoveElements();
            bl1vocex1.AddElements();
        }

        void check_click(object sender, EventArgs e)
        {
            foreach (var a in answers)
            {
                if (a.element.Text != a.answer)
                {
                    a.element.ForeColor = Color.Red;
                }
                else
                {
                    a.element.ForeColor = Color.Green;
                }
            }
        }

        void menu_click(object sender, EventArgs e)
        {
            bl1_voc_exes bl1vocexes = new bl1_voc_exes(mw);
            RemoveElements();
            bl1vocexes.AddElements();
        }
    }
}
