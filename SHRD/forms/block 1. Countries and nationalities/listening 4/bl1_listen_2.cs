﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;


namespace SHRD
{
    class bl1_listen_2
    {
        struct Answer
        {
            public TextBox element;
            public string answer;

            public Answer(TextBox tb, string text)
            {
                this.element = tb;
                this.answer = text;
            }
        }


        Button next;
        Button listen;
        Button back;
        Button check;
        PictureBox listening1;
        TextBox textBox1;
        TextBox textBox2;
        TextBox textBox3;
        TextBox textBox4;
        TextBox textBox5;
        TextBox textBox6;
        TextBox textBox7;
        TextBox textBox8;
        TextBox textBox9;
        TextBox textBox10;

        Form1 mw;
        FontLoader fl = new FontLoader();
        List<Answer> answers = new List<Answer>();


        public bl1_listen_2(Form1 form)
        {
            ///////////загрузка основного

            mw = form;

            mw.SuspendLayout();

            /////////////////////////////


            //Фоновая картинка
            listening1 = new PictureBox();
            listening1.Image = global::SHRD.Properties.Resources.ex24listen;
            listening1.Location = new Point(0, 0);
            listening1.Name = "listening1";
            listening1.TabIndex = 0;
            listening1.TabStop = false;
            listening1.Visible = true;
            listening1.Dock = DockStyle.Fill;
            listening1.SizeMode = PictureBoxSizeMode.StretchImage;
            //////

            //menu
            next = new Button();
            next.Location = new Point(845, 587);
            next.Size = new Size(210, 66);
            next.BackColor = Color.FromArgb(61, 155, 153);
            next.Font = fl.Load();
            next.Text = "Finish";
            next.Click += new EventHandler(next_click);

            //back
            back = new Button();
            back.Location = new Point(200, 587);
            back.Size = new Size(210, 66);
            back.BackColor = Color.FromArgb(61, 155, 153);
            back.Font = fl.Load();
            back.Text = "<<<Back";
            back.Click += new EventHandler(back_click);

            //check
            check = new Button();
            check.Location = new Point(630, 587);
            check.Size = new Size(210, 66);
            check.BackColor = Color.FromArgb(61, 155, 153);
            check.Font = fl.Load();
            check.Text = "Check";
            check.Click += new EventHandler(check_click);

            //listen
            listen = new Button();
            listen.Location = new Point(415, 587);
            listen.Size = new Size(210, 66);
            listen.BackColor = Color.FromArgb(61, 155, 153);
            listen.Font = fl.Load();
            listen.Text = "listen";
            listen.Click += new EventHandler(listen_click);

            /// 1
            textBox1 = new TextBox();
            textBox1.Location = new Point(310, 397);
            textBox1.Name = "textBox1";
            textBox1.Font = new Font("", 13F);
            textBox1.Size = new Size(150, 70);
            textBox1.BorderStyle = BorderStyle.None;
            textBox1.TextAlign = HorizontalAlignment.Center;

            //2
            textBox2 = new TextBox();
            textBox2.Location = new Point(657, 397);
            textBox2.Name = "textBox2";
            textBox2.Font = new Font("", 13F);
            textBox2.Size = new Size(90, 90);
            textBox2.BorderStyle = BorderStyle.None;
            textBox2.TextAlign = HorizontalAlignment.Center;


            //3
            textBox3 = new TextBox();
            textBox3.Location = new Point(304, 421);
            textBox3.Name = "textBox3";
            textBox3.Font = new Font("", 13F);
            textBox3.Size = new Size(150, 90);
            textBox3.BorderStyle = BorderStyle.None;
            textBox3.TextAlign = HorizontalAlignment.Center;


            //4
            textBox4 = new TextBox();
            textBox4.Location = new Point(736, 421);
            textBox4.Name = "textBox4";
            textBox4.Font = new Font("", 13F);
            textBox4.Size = new Size(100, 70);
            textBox4.BorderStyle = BorderStyle.None;
            textBox4.TextAlign = HorizontalAlignment.Center;


            //5
            textBox5 = new TextBox();
            textBox5.Location = new Point(760, 464);
            textBox5.Name = "textBox5";
            textBox5.Font = new Font("", 13F);
            textBox5.Size = new Size(150, 90);
            textBox5.BorderStyle = BorderStyle.None;
            textBox5.TextAlign = HorizontalAlignment.Center;

            //6
            textBox6 = new TextBox();
            textBox6.Location = new Point(390, 489);
            textBox6.Name = "textBox6";
            textBox6.Font = new Font("", 13F);
            textBox6.Size = new Size(150, 90);
            textBox6.BorderStyle = BorderStyle.None;
            textBox6.TextAlign = HorizontalAlignment.Center;

            //7
            textBox7 = new TextBox();
            textBox7.Location = new Point(764, 489);
            textBox7.Name = "textBox7";
            textBox7.Font = new Font("", 13F);
            textBox7.Size = new Size(150, 90);
            textBox7.BorderStyle = BorderStyle.None;
            textBox7.TextAlign = HorizontalAlignment.Center;

            //8
            textBox8 = new TextBox();
            textBox8.Location = new Point(273, 512);
            textBox8.Name = "textBox8";
            textBox8.Font = new Font("", 13F);
            textBox8.Size = new Size(140, 90);
            textBox8.BorderStyle = BorderStyle.None;
            textBox8.TextAlign = HorizontalAlignment.Center;

            //9
            textBox9 = new TextBox();
            textBox9.Location = new Point(600, 512);
            textBox9.Name = "textBox9";
            textBox9.Font = new Font("", 13F);
            textBox9.Size = new Size(160, 90);
            textBox9.BorderStyle = BorderStyle.None;
            textBox9.TextAlign = HorizontalAlignment.Center;

            //10
            textBox10 = new TextBox();
            textBox10.Location = new Point(455, 535);
            textBox10.Name = "textBox10";
            textBox10.Font = new Font("", 13F);
            textBox10.Size = new Size(155, 90);
            textBox10.BorderStyle = BorderStyle.None;
            textBox10.TextAlign = HorizontalAlignment.Center;

            mw.ResumeLayout();

            FillAnswerTable();
        }

        void FillAnswerTable()
        {
            if (answers.Count != 0)
            {
                answers.Clear();
            }


            //ОТВЕТЫ!!!!!!!!
            answers.Add(new Answer(textBox1, "first"));
            answers.Add(new Answer(textBox2, "surname"));
            answers.Add(new Answer(textBox3, "Russian"));
            answers.Add(new Answer(textBox4, "Hungary"));
            answers.Add(new Answer(textBox5, "Germany"));
            answers.Add(new Answer(textBox6, "wife"));
            answers.Add(new Answer(textBox7, "German"));
            answers.Add(new Answer(textBox8, "Hungarian"));
            answers.Add(new Answer(textBox9, "like"));
            answers.Add(new Answer(textBox10, "drink"));


        }

        public void AddElements()
        {
            mw.Controls.Add(textBox1);
            mw.Controls.Add(textBox2);
            mw.Controls.Add(textBox3);
            mw.Controls.Add(textBox4);
            mw.Controls.Add(textBox5);
            mw.Controls.Add(textBox6);
            mw.Controls.Add(textBox7);
            mw.Controls.Add(textBox8);
            mw.Controls.Add(textBox9);
            mw.Controls.Add(textBox10);
            mw.Controls.Add(next);
            mw.Controls.Add(listen);
            mw.Controls.Add(back);
            mw.Controls.Add(check);
            mw.Controls.Add(listening1);

        }

        public void RemoveElements()
        {
            mw.Controls.Remove(textBox1);
            mw.Controls.Remove(textBox2);
            mw.Controls.Remove(textBox3);
            mw.Controls.Remove(textBox4);
            mw.Controls.Remove(textBox5);
            mw.Controls.Remove(textBox6);
            mw.Controls.Remove(textBox7);
            mw.Controls.Remove(textBox8);
            mw.Controls.Remove(textBox9);
            mw.Controls.Remove(textBox10);
            mw.Controls.Remove(next);
            mw.Controls.Remove(check);
            mw.Controls.Remove(listen);
            mw.Controls.Remove(back);
            mw.Controls.Remove(check);
            mw.Controls.Remove(listening1);
        }

        System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"media\bl1\bl_1_listen_ex2.wav");

        void listen_click(object sender, EventArgs e)
        {
            //тут заглушка вместо диалога
            

            //прикрутить таймер надо для текста кнопки, чтобы listen на stop менялось, а то я так и не понял

            player.Play();

        }

        void back_click(object sender, EventArgs e)
        {
            bl1_listen_1 bl1listenex1 = new bl1_listen_1(mw);
            RemoveElements();
            bl1listenex1.AddElements();
            player.Stop();
        }

        void next_click(object sender, EventArgs e)
        {
            bl1_listen_exes bl1listenexes = new bl1_listen_exes(mw);
            RemoveElements();
            bl1listenexes.AddElements();
            player.Stop();
        }

        void check_click(object sender, EventArgs e)
        {
            foreach (var a in answers)
            {
                if (a.element.Text != a.answer)
                {
                    a.element.ForeColor = Color.Red;
                }
                else
                {
                    a.element.ForeColor = Color.Black;
                }
            }
        }
    }
}
