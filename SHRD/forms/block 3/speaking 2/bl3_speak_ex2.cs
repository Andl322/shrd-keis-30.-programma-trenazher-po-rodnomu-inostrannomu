﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SHRD
{
    class bl3_speak_ex2

    {
        //это для проверки ответов
        struct Answer
        {
            public TextBox element;
            public string answer;

            public Answer(TextBox tb, string text)
            {
                this.element = tb;
                this.answer = text;
            }
        }

        //нужно шобы кнопки и картинки работали
        Button next;
        Button listen;
        Button menu;
        Button check;
        PictureBox listening1;
        TextBox textBox1;
        TextBox textBox2;
        TextBox textBox3;
        TextBox textBox4;
        TextBox textBox5;
        TextBox textBox6;

        Form1 mw;
        FontLoader fl = new FontLoader();//шрифт грузим
        List<Answer> answers = new List<Answer>();//создает список для проверки ответов 

        public bl3_speak_ex2(Form1 form)
        {
            ///////////загрузка основного

            mw = form;

            mw.SuspendLayout();

            /////////////////////////////


            //Фоновая картинка
            listening1 = new PictureBox();
            listening1.Image = global::SHRD.Properties.Resources.bl3_speak_ex2;
            listening1.Location = new Point(0, 0);
            listening1.Name = "listening1";
            listening1.TabIndex = 0;
            listening1.TabStop = false;
            listening1.Visible = true;
            listening1.Dock = DockStyle.Fill;
            listening1.SizeMode = PictureBoxSizeMode.StretchImage;
            //////

            //next
            next = new Button();
            next.Location = new Point(845, 587);
            next.Size = new Size(210, 66);
            next.BackColor = Color.FromArgb(61, 155, 153);
            next.Font = fl.Load();
            next.Text = "Next>>>";
            next.Click += new EventHandler(next_click);

            //menu
            menu = new Button();
            menu.Location = new Point(200, 587);
            menu.Size = new Size(210, 66);
            menu.BackColor = Color.FromArgb(61, 155, 153);
            menu.Font = fl.Load();
            menu.Text = "<<<Back";
            menu.Click += new EventHandler(menu_click);

            //check
            check = new Button();
            check.Location = new Point(630, 587);
            check.Size = new Size(210, 66);
            check.BackColor = Color.FromArgb(61, 155, 153);
            check.Font = fl.Load();
            check.Text = "Check";
            check.Click += new EventHandler(check_click);

            //listen
            listen = new Button();
            listen.Location = new Point(415, 587);
            listen.Size = new Size(210, 66);
            listen.BackColor = Color.FromArgb(61, 155, 153);
            listen.Font = fl.Load();
            listen.Text = "Listen";
            listen.Click += new EventHandler(listen_click);

            /// 1
            textBox1 = new TextBox();
            textBox1.Location = new Point(435, 250);
            textBox1.Name = "textBox1";
            textBox1.Font = new Font("", 15F);
            textBox1.Size = new Size(90, 90);
            textBox1.BorderStyle = BorderStyle.None;
            textBox1.TextAlign = HorizontalAlignment.Center;

            //2
            textBox2 = new TextBox();
            textBox2.Location = new Point(606, 305);
            textBox2.Name = "textBox2";
            textBox2.Font = new Font("", 15F);
            textBox2.Size = new Size(70, 90);
            textBox2.BorderStyle = BorderStyle.None;
            textBox2.TextAlign = HorizontalAlignment.Center;

            //3
            textBox3 = new TextBox();
            textBox3.Location = new Point(380, 362);
            textBox3.Name = "textBox3";
            textBox3.Font = new Font("", 15F);
            textBox3.Size = new Size(90, 90);
            textBox3.BorderStyle = BorderStyle.None;
            textBox3.TextAlign = HorizontalAlignment.Center;

            //4
            textBox4 = new TextBox();
            textBox4.Location = new Point(485, 390);
            textBox4.Name = "textBox4";
            textBox4.Font = new Font("", 15F);
            textBox4.Size = new Size(90, 90);
            textBox4.BorderStyle = BorderStyle.None;
            textBox4.TextAlign = HorizontalAlignment.Center;

            //5
            textBox5 = new TextBox();
            textBox5.Location = new Point(485, 528);
            textBox5.Name = "textBox5";
            textBox5.Font = new Font("", 15F);
            textBox5.Size = new Size(70, 90);
            textBox5.BorderStyle = BorderStyle.None;
            textBox5.TextAlign = HorizontalAlignment.Center;

            //6
            textBox6 = new TextBox();
            textBox6.Location = new Point(760, 527);
            textBox6.Name = "textBox6";
            textBox6.Font = new Font("", 15F);
            textBox6.Size = new Size(70, 90);
            textBox6.BorderStyle = BorderStyle.None;
            textBox6.TextAlign = HorizontalAlignment.Center;

            mw.ResumeLayout();//это очень нужно 

            FillAnswerTable();//а это для проверки ответов

        }

        //проверка ответов
        void FillAnswerTable()
        {
            if (answers.Count != 0)
            {
                answers.Clear();
            }

            answers.Add(new Answer(textBox1, "take"));
            answers.Add(new Answer(textBox2, "front"));
            answers.Add(new Answer(textBox3, "here"));
            answers.Add(new Answer(textBox4, "much"));
            answers.Add(new Answer(textBox5, "change"));
            answers.Add(new Answer(textBox6, "nice"));



        }

        //отрисовка элементов в окне
        public void AddElements()
        {
            mw.Controls.Add(textBox1);
            mw.Controls.Add(textBox2);
            mw.Controls.Add(textBox3);
            mw.Controls.Add(textBox4);
            mw.Controls.Add(textBox5);
            mw.Controls.Add(textBox6);
            mw.Controls.Add(next);
            mw.Controls.Add(listen);
            mw.Controls.Add(menu);
            mw.Controls.Add(check);
            mw.Controls.Add(listening1);

        }

        //удаляет элементы
        public void RemoveElements()
        {
            mw.Controls.Remove(textBox1);
            mw.Controls.Remove(textBox2);
            mw.Controls.Remove(textBox3);
            mw.Controls.Remove(textBox4);
            mw.Controls.Remove(textBox5);
            mw.Controls.Remove(textBox6);
            mw.Controls.Remove(next);
            mw.Controls.Remove(check);
            mw.Controls.Remove(listen);
            mw.Controls.Remove(menu);
            mw.Controls.Remove(check);
            mw.Controls.Remove(listening1);
        }

        //событие по нажатию кнопки menu
        void menu_click(object sender, EventArgs e)
        {
            bl3_speak_ex1 bl1listenexes = new bl3_speak_ex1(mw);
            RemoveElements();
            bl1listenexes.AddElements();
            player.Stop();
        }


        //загружает аудио
        System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"media\bl3\bl3_speak_ex2.wav");



        void listen_click(object sender, EventArgs e)
        {
            //тут заглушка вместо диалога

            //прикрутить таймер надо для текста кнопки, чтобы listen на stop менялось, а то я так и не понял

            player.Play();//проигрывать аудио

        }

        void next_click(object sender, EventArgs e)
        {
            bl3_speak_ex3 bl3speakex3 = new bl3_speak_ex3(mw);//создаем локальную переменную в классе для обращения к другому классу
            RemoveElements();//убрать элементы
            bl3speakex3.AddElements();//добавить элементы из другого класса через локальную переменную
            player.Stop();//остановка прослушивания
        }

        //это штука проверяет ответы по нажатию кнопки 
        void check_click(object sender, EventArgs e)
        {
            foreach (var a in answers)
            {
                if (a.element.Text != a.answer)
                {
                    a.element.ForeColor = Color.Red;
                }
                else
                {
                    a.element.ForeColor = Color.Green;
                }
            }
        }
    }
}