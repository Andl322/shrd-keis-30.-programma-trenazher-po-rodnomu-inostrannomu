﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Media;


namespace SHRD
{
    class bl3_speak_ex3
    {
        //добавляем элементы
        Button finish;
        Button listen;
        Button menu;
        PictureBox reading1;

        Form1 mw;
        FontLoader fl = new FontLoader();//шрифт грузим




        public bl3_speak_ex3(Form1 form)
        {

            ///////////загрузка основного

            mw = form;

            mw.SuspendLayout();

            /////////////////////////////

            //Фоновая картинка
            reading1 = new PictureBox();
            reading1.Image = global::SHRD.Properties.Resources.bl3_speak_ex3;
            reading1.Location = new Point(0, 0);
            reading1.Name = "reading1";
            reading1.TabIndex = 0;
            reading1.TabStop = false;
            reading1.Visible = true;
            reading1.Dock = DockStyle.Fill;
            reading1.SizeMode = PictureBoxSizeMode.StretchImage;
            //////

            //finish
            finish = new Button();
            finish.Location = new Point(725, 587);
            finish.Size = new Size(210, 66);
            finish.BackColor = Color.FromArgb(61, 155, 153);
            finish.Font = fl.Load();
            finish.Text = "Finish";
            finish.Click += new EventHandler(finish_click);//обращение к классу NEXT_CLICK при нажатии кнопки

            //listen
            listen = new Button();
            listen.Location = new Point(510, 587);
            listen.Size = new Size(210, 66);
            listen.BackColor = Color.FromArgb(61, 155, 153);
            listen.Font = fl.Load();
            listen.Text = "Listen";
            listen.Click += new EventHandler(listen_click);


            //
            menu = new Button();
            menu.Location = new Point(295, 587);
            menu.Size = new Size(210, 66);
            menu.BackColor = Color.FromArgb(61, 155, 153);
            menu.Font = fl.Load();
            menu.Text = "<<<Back";
            menu.Click += new EventHandler(menu_click);




            mw.ResumeLayout();//это тоже нужно


        }


        //выводит картинки и кнопки на экран
        public void AddElements()
        {
            mw.Controls.Add(finish);
            mw.Controls.Add(listen);
            mw.Controls.Add(menu);
            mw.Controls.Add(reading1);
        }

        //удаляет их
        public void RemoveElements()
        {
            mw.Controls.Remove(finish);
            mw.Controls.Remove(listen);
            mw.Controls.Remove(menu);
            mw.Controls.Remove(reading1);
        }

        //событие по нажатию кнопки
        void finish_click(object sender, EventArgs e)
        {
            bl3_speak_exes bl3voc = new bl3_speak_exes(mw);
            RemoveElements();
            bl3voc.AddElements();
            player.Stop();
        }

        System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"media\bl3\bl3_speak_ex3.wav");

        public void listen_click(object sender, EventArgs e)
        {
            //тут заглушка вместо диалога




            player.Play();

        }

        void menu_click(object sender, EventArgs e)
        {
            bl3_speak_ex2 bl3speak2 = new bl3_speak_ex2(mw);
            RemoveElements();
            bl3speak2.AddElements();
            player.Stop();
        }


    }


}
