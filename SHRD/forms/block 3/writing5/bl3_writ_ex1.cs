﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SHRD
{
    class bl3_writ_ex1
    {


        struct Answer
        {
            public TextBox element;
            public string answer;

            public Answer(TextBox tb, string text)
            {
                this.element = tb;
                this.answer = text;
            }
        }


        Button menu;
        Button back;
        Button show;
        Button check;
        Button text;
        PictureBox reading1;
        PictureBox dialog;
        TextBox textBox1;
        TextBox textBox2;
        TextBox textBox3;
        TextBox textBox4;
        TextBox textBox5;
        TextBox textBox6;
        TextBox textBox7;

        Form1 mw;
        FontLoader fl = new FontLoader();
        List<Answer> answers = new List<Answer>();

        public bl3_writ_ex1(Form1 form)
        {
            ///////////загрузка основного

            mw = form;

            mw.SuspendLayout();

            /////////////////////////////



            //Фоновая картинка
            reading1 = new PictureBox();
            reading1.Image = global::SHRD.Properties.Resources.bl3_writ_ex1;
            reading1.Location = new Point(0, 0);
            reading1.Name = "reading1";
            reading1.TabIndex = 0;
            reading1.TabStop = false;
            reading1.Visible = true;
            reading1.Dock = DockStyle.Fill;
            reading1.SizeMode = PictureBoxSizeMode.StretchImage;
            //////


            //диалог
            dialog = new PictureBox();
            dialog.Image = global::SHRD.Properties.Resources.bl3_writ_ex1_1;
            dialog.Location = new Point(0, 0);
            dialog.Name = "dialog";
            dialog.TabIndex = 0;
            dialog.TabStop = false;
            //dialog.Visible = true;
            dialog.Dock = DockStyle.Fill;
            dialog.SizeMode = PictureBoxSizeMode.StretchImage;




            //menu
            menu = new Button();
            menu.Location = new Point(845, 587);
            menu.Size = new Size(210, 66);
            menu.BackColor = Color.FromArgb(61, 155, 153);
            menu.Font = fl.Load();
            menu.Text = "Finish";
            menu.Click += new EventHandler(menu_click);

            //text
            text = new Button();
            text.Location = new Point(630, 587);
            text.Size = new Size(210, 66);
            text.BackColor = Color.FromArgb(61, 155, 153);
            text.Font = fl.Load();
            text.Text = "Text";
            text.Click += new EventHandler(text_click);

            //check
            check = new Button();
            check.Location = new Point(415, 587);
            check.Size = new Size(210, 66);
            check.BackColor = Color.FromArgb(61, 155, 153);
            check.Font = fl.Load();
            check.Text = "Check";
            check.Click += new EventHandler(check_click);

            //back
            back = new Button();
            back.Location = new Point(200, 587);
            back.Size = new Size(210, 66);
            back.BackColor = Color.FromArgb(61, 155, 153);
            back.Font = fl.Load();
            back.Text = "<<<Back";
            back.Click += new EventHandler(back_click);

            //show dialog
            show = new Button();
            show.Location = new Point(200, 587);
            show.Size = new Size(210, 66);
            show.BackColor = Color.FromArgb(61, 155, 153);
            show.Font = fl.Load();
            show.Text = "Hide";
            show.Click += new EventHandler(show_click);



            /// 1
            textBox1 = new TextBox();
            textBox1.Location = new Point(365, 240);
            textBox1.Name = "textBox1";
            textBox1.Font = new Font("", 17F);
            textBox1.Size = new Size(875, 70);
            textBox1.BorderStyle = BorderStyle.None;
            textBox1.TextAlign = HorizontalAlignment.Center;

            //2
            textBox2 = new TextBox();
            textBox2.Location = new Point(365, 295);
            textBox2.Name = "textBox2";
            textBox2.Font = new Font("", 15F);
            textBox2.Size = new Size(875, 90);
            textBox2.BorderStyle = BorderStyle.None;
            textBox2.TextAlign = HorizontalAlignment.Center;


            //3
            textBox3 = new TextBox();
            textBox3.Location = new Point(240, 343);
            textBox3.Name = "textBox3";
            textBox3.Font = new Font("", 15F);
            textBox3.Size = new Size(1000, 90);
            textBox3.BorderStyle = BorderStyle.None;
            textBox3.TextAlign = HorizontalAlignment.Center;


            //4
            textBox4 = new TextBox();
            textBox4.Location = new Point(295, 393);
            textBox4.Name = "textBox4";
            textBox4.Font = new Font("", 15F);
            textBox4.Size = new Size(940, 100);
            textBox4.BorderStyle = BorderStyle.None;
            textBox4.TextAlign = HorizontalAlignment.Center;


            //5
            textBox5 = new TextBox();
            textBox5.Location = new Point(333, 440);
            textBox5.Name = "textBox5";
            textBox5.Font = new Font("", 15F);
            textBox5.Size = new Size(907, 90);
            textBox5.BorderStyle = BorderStyle.None;
            textBox5.TextAlign = HorizontalAlignment.Center;

            //6
            textBox6 = new TextBox();
            textBox6.Location = new Point(340, 487);
            textBox6.Name = "textBox6";
            textBox6.Font = new Font("", 15F);
            textBox6.Size = new Size(900, 90);
            textBox6.BorderStyle = BorderStyle.None;
            textBox6.TextAlign = HorizontalAlignment.Center;

            //7
            textBox7 = new TextBox();
            textBox7.Location = new Point(675, 438);
            textBox7.Name = "textBox7";
            textBox7.Font = new Font("", 15F);
            textBox7.Size = new Size(450, 90);
            textBox7.BorderStyle = BorderStyle.None;
            textBox7.TextAlign = HorizontalAlignment.Center;



            mw.ResumeLayout();

            FillAnswerTable();



        }

        void FillAnswerTable()
        {
            if (answers.Count != 0)
            {
                answers.Clear();
            }

            


        }


        public void AddElements()
        {

            mw.Controls.Add(textBox1);
            mw.Controls.Add(textBox2);
            mw.Controls.Add(textBox3);
            mw.Controls.Add(textBox4);
            mw.Controls.Add(textBox5);
            mw.Controls.Add(textBox6);
            mw.Controls.Add(textBox7);
            mw.Controls.Add(check);
            mw.Controls.Add(text);
            mw.Controls.Add(menu);
            mw.Controls.Add(back);
            //mw.Controls.Add(dialog);
            mw.Controls.Add(reading1);


        }

        public void RemoveElements()
        {
            mw.Controls.Remove(check);
            mw.Controls.Remove(text);
            mw.Controls.Remove(menu);
            mw.Controls.Remove(back);
            mw.Controls.Remove(reading1);
            mw.Controls.Remove(textBox1);
            mw.Controls.Remove(textBox2);
            mw.Controls.Remove(textBox3);
            mw.Controls.Remove(textBox4);
            mw.Controls.Remove(textBox5);
            mw.Controls.Remove(textBox6);
            mw.Controls.Remove(textBox7);
        }

        public void HideElements()
        {
            check.Visible = false;
            text.Visible = false;
            menu.Visible = false;
            back.Visible = false;

            mw.Controls.Add(show);
            mw.Controls.Add(dialog);

            reading1.Visible = false;
            dialog.BringToFront();
            show.BringToFront();

            /*
            textBox1.Location = new Point (675, +1500);
            textBox2.Location = new Point(675, +1500);
            textBox3.Location = new Point(675, +1500);
            textBox4.Location = new Point(675, +1200);
            textBox5.Location = new Point(675, +1200);
            textBox6.Location = new Point(675, +1200);
            textBox7.Location = new Point(675, +1200);
            */
        }





        public void ShowElements()
        {



            //show.Visible = false;
            check.Visible = true;
            text.Visible = true;
            menu.Visible = true;
            back.Visible = true;
            mw.Controls.Remove(show);
            mw.Controls.Remove(dialog);
            /*
            textBox1.Location = new Point (675,-1500);
            textBox2.Location = new Point(675, -1500);
            textBox3.Location = new Point(675, -1500);
            textBox4.Location = new Point(675, -1200);
            textBox5.Location = new Point(675, -1200);
            textBox6.Location = new Point(675, -1200);
            textBox7.Location = new Point(675, -1200);
            */

            reading1.Visible = true;



        }

        public int a;

        void text_click(object sender, EventArgs e)
        {
            HideElements();
        }

        void back_click(object sender, EventArgs e)
        {
            bl3_writ_exes bl3wrex = new bl3_writ_exes(mw);
            RemoveElements();
            bl3wrex.AddElements();
            //HideElements();
            a = 1;
        }



        void check_click(object sender, EventArgs e)
        {
            foreach (var a in answers)
            {
                if (a.element.Text != a.answer)
                {
                    a.element.ForeColor = Color.Red;
                }
                else
                {
                    a.element.ForeColor = Color.Green;
                }
            }
        }

        void menu_click(object sender, EventArgs e)
        {
            bl3_writ_exes bl3wrex = new bl3_writ_exes(mw);
            RemoveElements();
            bl3wrex.AddElements();
            //HideElements();
        }

        void show_click(object sender, EventArgs e)
        {
            ShowElements();
        }
    }
}
