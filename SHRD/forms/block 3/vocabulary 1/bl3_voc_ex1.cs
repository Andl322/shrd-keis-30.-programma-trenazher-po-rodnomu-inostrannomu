﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Media;


namespace SHRD
{
    class bl3_voc_ex1
    {
        //добавляем элементы
        Button next;
        Button listen;
        Button menu;
        PictureBox reading1;

        Form1 mw;
        FontLoader fl = new FontLoader();//шрифт грузим




        public bl3_voc_ex1(Form1 form)
        {

            ///////////загрузка основного

            mw = form;

            mw.SuspendLayout();

            /////////////////////////////

            //Фоновая картинка
            reading1 = new PictureBox();
            reading1.Image = global::SHRD.Properties.Resources.bl3_voc_ex1;
            reading1.Location = new Point(0, 0);
            reading1.Name = "reading1";
            reading1.TabIndex = 0;
            reading1.TabStop = false;
            reading1.Visible = true;
            reading1.Dock = DockStyle.Fill;
            reading1.SizeMode = PictureBoxSizeMode.StretchImage;
            //////

            //next
            next = new Button();
            next.Location = new Point(725, 587);
            next.Size = new Size(210, 66);
            next.BackColor = Color.FromArgb(61, 155, 153);
            next.Font = fl.Load();
            next.Text = "Next>>>";
            next.Click += new EventHandler(next_click);//обращение к классу NEXT_CLICK при нажатии кнопки

            //listen
            listen = new Button();
            listen.Location = new Point(510, 587);
            listen.Size = new Size(210, 66);
            listen.BackColor = Color.FromArgb(61, 155, 153);
            listen.Font = fl.Load();
            listen.Text = "Listen";
            listen.Click += new EventHandler(listen_click);


            //menu
            menu = new Button();
            menu.Location = new Point(295, 587);
            menu.Size = new Size(210, 66);
            menu.BackColor = Color.FromArgb(61, 155, 153);
            menu.Font = fl.Load();
            menu.Text = "Menu";
            menu.Click += new EventHandler(menu_click);




            mw.ResumeLayout();//это тоже нужно


        }


        //выводит картинки и кнопки на экран
        public void AddElements()
        {
            mw.Controls.Add(next);
            mw.Controls.Add(listen);
            mw.Controls.Add(menu);
            mw.Controls.Add(reading1);
        }

        //удаляет их
        public void RemoveElements()
        {
            mw.Controls.Remove(next);
            mw.Controls.Remove(listen);
            mw.Controls.Remove(menu);
            mw.Controls.Remove(reading1);
        }

        //событие по нажатию кнопки
        void next_click(object sender, EventArgs e)
        {
            bl3_voc_ex2 bl3vocex2 = new bl3_voc_ex2(mw);
            RemoveElements();
            bl3vocex2.AddElements();
            player.Stop();
        }

        System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"media\bl3\bl3_voc_ex1.wav");

        public void listen_click(object sender, EventArgs e)
        {
            //тут заглушка вместо диалога


            

            player.Play();

        }

        void menu_click(object sender, EventArgs e)
        {
            bl3_voc_exes bl3voc = new bl3_voc_exes(mw);
            RemoveElements();
            bl3voc.AddElements();
            player.Stop();
        }


    }


}
