﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SHRD
{
    class bl3_listen_ex3

    {
        //это для проверки ответов
        struct Answer
        {
            public ComboBox element;
            public string answer;

            public Answer(ComboBox tb, string text)
            {
                this.element = tb;
                this.answer = text;
            }
        }

        //нужно шобы кнопки и картинки работали
        Button finish;
        Button listen;
        Button menu;
        Button check;
        PictureBox listening1;
        ComboBox comboBox1;
        ComboBox comboBox2;
        ComboBox comboBox3;
        ComboBox comboBox4;
        ComboBox comboBox5;

        Form1 mw;
        FontLoader fl = new FontLoader();//шрифт грузим
        List<Answer> answers = new List<Answer>();//создает список для проверки ответов 

        public bl3_listen_ex3(Form1 form)
        {
            ///////////загрузка основного

            mw = form;

            mw.SuspendLayout();

            /////////////////////////////


            //Фоновая картинка
            listening1 = new PictureBox();
            listening1.Image = global::SHRD.Properties.Resources.bl3_listen_ex3;
            listening1.Location = new Point(0, 0);
            listening1.Name = "listening1";
            listening1.TabIndex = 0;
            listening1.TabStop = false;
            listening1.Visible = true;
            listening1.Dock = DockStyle.Fill;
            listening1.SizeMode = PictureBoxSizeMode.StretchImage;
            //////

            //Finish
            finish = new Button();
            finish.Location = new Point(845, 587);
            finish.Size = new Size(210, 66);
            finish.BackColor = Color.FromArgb(61, 155, 153);
            finish.Font = fl.Load();
            finish.Text = "Finish";
            finish.Click += new EventHandler(finish_click);

            //menu
            menu = new Button();
            menu.Location = new Point(200, 587);
            menu.Size = new Size(210, 66);
            menu.BackColor = Color.FromArgb(61, 155, 153);
            menu.Font = fl.Load();
            menu.Text = "<<<Back";
            menu.Click += new EventHandler(menu_click);

            //check
            check = new Button();
            check.Location = new Point(630, 587);
            check.Size = new Size(210, 66);
            check.BackColor = Color.FromArgb(61, 155, 153);
            check.Font = fl.Load();
            check.Text = "Check";
            check.Click += new EventHandler(check_click);

            //listen
            listen = new Button();
            listen.Location = new Point(415, 587);
            listen.Size = new Size(210, 66);
            listen.BackColor = Color.FromArgb(61, 155, 153);
            listen.Font = fl.Load();
            listen.Text = "Listen";
            listen.Click += new EventHandler(listen_click);

            /// 1
            comboBox1 = new ComboBox();
            comboBox1.Location = new Point(1082, 250);
            comboBox1.Name = "comboBox1";
            comboBox1.Items.Add  ("True");
            comboBox1.Items.Add ("False");
            comboBox1.Font = new Font("", 17F);
            comboBox1.Size = new Size(90, 90);

            //2
            comboBox2 = new ComboBox();
            comboBox2.Location = new Point(1082, 300);
            comboBox2.Name = "comboBox2";
            comboBox2.Items.Add("True");
            comboBox2.Items.Add("False");
            comboBox2.Font = new Font("", 17F);
            comboBox2.Size = new Size(90, 90);

            //3
            comboBox3 = new ComboBox();
            comboBox3.Location = new Point(1082, 350);
            comboBox3.Name = "comboBox3";
            comboBox3.Items.Add("True");
            comboBox3.Items.Add("False");
            comboBox3.Font = new Font("", 17F);
            comboBox3.Size = new Size(90, 90);

            //4
            comboBox4 = new ComboBox();
            comboBox4.Location = new Point(1082, 400);
            comboBox4.Name = "comboBox4";
            comboBox4.Items.Add("True");
            comboBox4.Items.Add("False");
            comboBox4.Font = new Font("", 17F);
            comboBox4.Size = new Size(90, 90);

            //5
            comboBox5 = new ComboBox();
            comboBox5.Location = new Point(1082, 450);
            comboBox5.Name = "comboBox5";
            comboBox5.Items.Add("True");
            comboBox5.Items.Add("False");
            comboBox5.Font = new Font("", 17F);
            comboBox5.Size = new Size(90, 90);


            mw.ResumeLayout();//это очень нужно 

            FillAnswerTable();//а это для проверки ответов

        }

        //проверка ответов
        void FillAnswerTable()
        {
            if (answers.Count != 0)
            {
                answers.Clear();
            }
            answers.Add(new Answer(comboBox1, "False"));
            answers.Add(new Answer(comboBox2, "True"));
            answers.Add(new Answer(comboBox3, "True"));
            answers.Add(new Answer(comboBox4, "False"));
            answers.Add(new Answer(comboBox5, "False"));





        }

        //отрисовка элементов в окне
        public void AddElements()
        {
            mw.Controls.Add(comboBox1);
            mw.Controls.Add(comboBox2);
            mw.Controls.Add(comboBox3);
            mw.Controls.Add(comboBox4);
            mw.Controls.Add(comboBox5);
            mw.Controls.Add(finish);
            mw.Controls.Add(listen);
            mw.Controls.Add(menu);
            mw.Controls.Add(check);
            mw.Controls.Add(listening1);

        }

        //удаляет элементы
        public void RemoveElements()
        {
            mw.Controls.Remove(comboBox1);
            mw.Controls.Remove(comboBox2);
            mw.Controls.Remove(comboBox3);
            mw.Controls.Remove(comboBox4);
            mw.Controls.Remove(comboBox5);
            mw.Controls.Remove(check);
            mw.Controls.Remove(finish);
            mw.Controls.Remove(listen);
            mw.Controls.Remove(menu);
            mw.Controls.Remove(check);
            mw.Controls.Remove(listening1);
        }

        //событие по нажатию кнопки menu
        void menu_click(object sender, EventArgs e)
        {
            bl3_listen_ex2 bl1listenexes = new bl3_listen_ex2(mw);
            RemoveElements();
            bl1listenexes.AddElements();
            player.Stop();
        }


        //загружает аудио
        System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"media\bl3\bl3_listen_ex3.wav");



        void listen_click(object sender, EventArgs e)
        {
            //тут заглушка вместо диалога

            //прикрутить таймер надо для текста кнопки, чтобы listen на stop менялось, а то я так и не понял

            player.Play();//проигрывать аудио

        }

        void finish_click (object sender, EventArgs e)
        {
            bl3_listen_exes bl3lis = new bl3_listen_exes(mw);//создаем локальную переменную в классе для обращения к другому классу
            RemoveElements();//убрать элементы
            bl3lis.AddElements();//добавить элементы из другого класса через локальную переменную
            player.Stop();//остановка прослушивания
        }

        //это штука проверяет ответы по нажатию кнопки 
        void check_click(object sender, EventArgs e)
        {
            foreach (var a in answers)
            {
                if (a.element.Text != a.answer)
                {
                    a.element.ForeColor = Color.Red;
                }
                else
                {
                    a.element.ForeColor = Color.Green;
                }
            }
        }
    }
}