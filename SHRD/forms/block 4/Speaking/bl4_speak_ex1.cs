﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SHRD
{
    class bl4_speak_ex1

    {
        //это для проверки ответов
        struct Answer
        {
            public TextBox element;
            public string answer;

            public Answer(TextBox cb, string text)
            {
                this.element = cb;
                this.answer = text;
            }
        }

        //нужно шобы кнопки и картинки работали
        Button next;
        Button listen;
        Button menu;
        Button check;
        PictureBox listening1;
        CheckBox checkBox1;
        CheckBox checkBox2;
        CheckBox checkBox3;
        CheckBox checkBox4;
        CheckBox checkBox5;
        CheckBox checkBox6;
        CheckBox checkBox7;
        CheckBox checkBox8;
        CheckBox checkBox9;
        CheckBox checkBox10;
        CheckBox checkBox11;
        CheckBox checkBox12;

        Form1 mw;
        FontLoader fl = new FontLoader();//шрифт грузим
        List<Answer> answers = new List<Answer>();//создает список для проверки ответов 

        public bl4_speak_ex1(Form1 form)
        {
            ///////////загрузка основного

            mw = form;

            mw.SuspendLayout();

            /////////////////////////////


            //Фоновая картинка
            listening1 = new PictureBox();
            listening1.Image = global::SHRD.Properties.Resources.bl4_speak_ex1;
            listening1.Location = new Point(0, 0);
            listening1.Name = "listening1";
            listening1.TabIndex = 0;
            listening1.TabStop = false;
            listening1.Visible = true;
            listening1.Dock = DockStyle.Fill;
            listening1.SizeMode = PictureBoxSizeMode.StretchImage;
            //////

            //next
            next = new Button();
            next.Location = new Point(845, 587);
            next.Size = new Size(210, 66);
            next.BackColor = Color.FromArgb(61, 155, 153);
            next.Font = fl.Load();
            next.Text = "Next>>>";
            next.Click += new EventHandler(next_click);

            //menu
            menu = new Button();
            menu.Location = new Point(200, 587);
            menu.Size = new Size(210, 66);
            menu.BackColor = Color.FromArgb(61, 155, 153);
            menu.Font = fl.Load();
            menu.Text = "Menu";
            menu.Click += new EventHandler(menu_click);

            //check
            check = new Button();
            check.Location = new Point(630, 587);
            check.Size = new Size(210, 66);
            check.BackColor = Color.FromArgb(61, 155, 153);
            check.Font = fl.Load();
            check.Text = "Check";
            check.Click += new EventHandler(check_click);

            //listen
            listen = new Button();
            listen.Location = new Point(415, 587);
            listen.Size = new Size(210, 66);
            listen.BackColor = Color.FromArgb(61, 155, 153);
            listen.Font = fl.Load();
            listen.Text = "Listen";
            listen.Click += new EventHandler(listen_click);

            /// 1
            checkBox1 = new CheckBox();
            checkBox1.Location = new Point(1003, 365);
            checkBox1.Name = "checkBox1";
            checkBox1.AutoSize = true;

            //2
            checkBox2 = new CheckBox();
            checkBox2.Location = new Point(1003, 393);
            checkBox2.Name = "checkBox2";
            checkBox2.AutoSize = true;

            //3
            checkBox3 = new CheckBox();
            checkBox3.Location = new Point(1003, 421);
            checkBox3.Name = "checkBox3";
            checkBox3.AutoSize = true;

            //4
            checkBox4 = new CheckBox();
            checkBox4.Location = new Point(1003, 452);
            checkBox4.Name = "checkBox4";
            checkBox4.AutoSize = true;

            //5
            checkBox5 = new CheckBox();
            checkBox5.Location = new Point(1003, 482);
            checkBox5.Name = "checkBox5";
            checkBox5.AutoSize = true;

            //6
            checkBox6 = new CheckBox();
            checkBox6.Location = new Point(1003, 512);
            checkBox6.Name = "checkBox6";
            checkBox6.AutoSize = true;

            //7
            checkBox7 = new CheckBox();
            checkBox7.Location = new Point(1239, 365);
            checkBox7.Name = "checkBox7";
            checkBox7.AutoSize = true;

            //8
            checkBox8 = new CheckBox();
            checkBox8.Location = new Point(1239, 393);
            checkBox8.Name = "checkBox8";
            checkBox8.AutoSize = true;

            //9
            checkBox9 = new CheckBox();
            checkBox9.Location = new Point(1239, 421);
            checkBox9.Name = "checkBox9";
            checkBox9.AutoSize = true;

            //10
            checkBox10 = new CheckBox();
            checkBox10.Location = new Point(1239, 452);
            checkBox10.Name = "checkBox10";
            checkBox10.AutoSize = true;

            //11
            checkBox11 = new CheckBox();
            checkBox11.Location = new Point(1239, 482);
            checkBox11.Name = "checkBox11";
            checkBox11.AutoSize = true;

            //12
            checkBox12 = new CheckBox();
            checkBox12.Location = new Point(1239, 512);
            checkBox12.Name = "checkBox12";
            checkBox12.AutoSize = true;

            mw.ResumeLayout();//это очень нужно 

            FillAnswerTable();//а это для проверки ответов

        }

        //проверка ответов
        void FillAnswerTable()
        {
            if (answers.Count != 0)
            {
                answers.Clear();
            }





        }

        //отрисовка элементов в окне
        public void AddElements()
        {
            mw.Controls.Add(checkBox1);
            mw.Controls.Add(checkBox2);
            mw.Controls.Add(checkBox3);
            mw.Controls.Add(checkBox4);
            mw.Controls.Add(checkBox5);
            mw.Controls.Add(checkBox6);
            mw.Controls.Add(checkBox7);
            mw.Controls.Add(checkBox8);
            mw.Controls.Add(checkBox9);
            mw.Controls.Add(checkBox10);
            mw.Controls.Add(checkBox11);
            mw.Controls.Add(checkBox12);
            mw.Controls.Add(next);
            mw.Controls.Add(listen);
            mw.Controls.Add(menu);
            mw.Controls.Add(check);
            mw.Controls.Add(listening1);

        }

        //удаляет элементы
        public void RemoveElements()
        {
            mw.Controls.Remove(checkBox1);
            mw.Controls.Remove(checkBox2);
            mw.Controls.Remove(checkBox3);
            mw.Controls.Remove(checkBox4);
            mw.Controls.Remove(checkBox5);
            mw.Controls.Remove(checkBox6);
            mw.Controls.Remove(checkBox7);
            mw.Controls.Remove(checkBox8);
            mw.Controls.Remove(checkBox9);
            mw.Controls.Remove(checkBox10);
            mw.Controls.Remove(checkBox11);
            mw.Controls.Remove(checkBox12);
            mw.Controls.Remove(next);
            mw.Controls.Remove(check);
            mw.Controls.Remove(listen);
            mw.Controls.Remove(menu);
            mw.Controls.Remove(check);
            mw.Controls.Remove(listening1);
        }

        //событие по нажатию кнопки menu
        void menu_click(object sender, EventArgs e)
        {
            bl4_speak_exes bl4lisex = new bl4_speak_exes(mw);
            RemoveElements();
            bl4lisex.AddElements();
            player.Stop();
        }


        //загружает аудио
        System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"media\bl4\bl4_speak_ex1.wav");



        void listen_click(object sender, EventArgs e)
        {
            //тут заглушка вместо диалога

            //прикрутить таймер надо для текста кнопки, чтобы listen на stop менялось, а то я так и не понял

            player.Play();//проигрывать аудио

        }

        void next_click(object sender, EventArgs e)
        {
            bl4_speak_ex2 bl4spex = new bl4_speak_ex2(mw);//создаем локальную переменную в классе для обращения к другому классу
            RemoveElements();//убрать элементы
            bl4spex.AddElements();//добавить элементы из другого класса через локальную переменную
            player.Stop();//остановка прослушивания
        }

        //это штука проверяет ответы по нажатию кнопки 
        void check_click(object sender, EventArgs e)
        {

           if (checkBox1.Checked == true && checkBox2.Checked == false && checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == true && checkBox6.Checked == true &&
                checkBox7.Checked == true && checkBox8.Checked == true && checkBox9.Checked == false && checkBox10.Checked == true && checkBox11.Checked ==false && checkBox12.Checked == false
                )
            {
                MessageBox.Show("You answered correctly!!!");
            }
           else
            {
                MessageBox.Show("The answers contains mistakes. Be careful!");
            }
          
            foreach (var a in answers)
            {
                if (a.element.Text != a.answer)
                {
                    a.element.ForeColor = Color.Red;
                }
                else
                {
                    a.element.ForeColor = Color.Green;
                }
            }
        }
    }
}