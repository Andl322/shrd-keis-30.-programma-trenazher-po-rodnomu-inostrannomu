﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SHRD
{
    class bl4_write_ex1

    {
        //это для проверки ответов
        struct Answer
        {
            public TextBox element;
            public string answer;

            public Answer(TextBox tb, string text)
            {
                this.element = tb;
                this.answer = text;
            }
        }

        //нужно шобы кнопки и картинки работали
        Button next;
        Button listen;
        Button menu;
        Button check;
        PictureBox listening1;
        TextBox textBox1;
        TextBox textBox2;
        TextBox textBox3;
        TextBox textBox4;
        TextBox textBox5;
        TextBox textBox6;
        TextBox textBox7;
        TextBox textBox8;
        TextBox textBox9;
        TextBox textBox10;

        Form1 mw;
        FontLoader fl = new FontLoader();//шрифт грузим
        List<Answer> answers = new List<Answer>();//создает список для проверки ответов 

        public bl4_write_ex1(Form1 form)
        {
            ///////////загрузка основного

            mw = form;

            mw.SuspendLayout();

            /////////////////////////////


            //Фоновая картинка
            listening1 = new PictureBox();
            listening1.Image = global::SHRD.Properties.Resources.bl4_write_ex1;
            listening1.Location = new Point(0, 0);
            listening1.Name = "listening1";
            listening1.TabIndex = 0;
            listening1.TabStop = false;
            listening1.Visible = true;
            listening1.Dock = DockStyle.Fill;
            listening1.SizeMode = PictureBoxSizeMode.StretchImage;
            //////

            //next
            next = new Button();
            next.Location = new Point(845, 587);
            next.Size = new Size(210, 66);
            next.BackColor = Color.FromArgb(61, 155, 153);
            next.Font = fl.Load();
            next.Text = "Finish";
            next.Click += new EventHandler(next_click);

            //menu
            menu = new Button();
            menu.Location = new Point(200, 587);
            menu.Size = new Size(210, 66);
            menu.BackColor = Color.FromArgb(61, 155, 153);
            menu.Font = fl.Load();
            menu.Text = "<<<Back";
            menu.Click += new EventHandler(menu_click);

            //check
            check = new Button();
            check.Location = new Point(630, 587);
            check.Size = new Size(210, 66);
            check.BackColor = Color.FromArgb(61, 155, 153);
            check.Font = fl.Load();
            check.Text = "Check";
            check.Click += new EventHandler(check_click);

            //listen
            listen = new Button();
            listen.Location = new Point(415, 587);
            listen.Size = new Size(210, 66);
            listen.BackColor = Color.FromArgb(61, 155, 153);
            listen.Font = fl.Load();
            listen.Text = "Listen";
            listen.Click += new EventHandler(listen_click);

            /// 1
            textBox1 = new TextBox();
            textBox1.Location = new Point(914, 159);
            textBox1.Name = "textBox1";
            textBox1.Font = new Font("", 17F);
            textBox1.Size = new Size(315, 15);
            textBox1.BorderStyle = BorderStyle.None;            
            textBox1.TextAlign = HorizontalAlignment.Center;

            //2
            textBox2 = new TextBox();
            textBox2.Location = new Point(914, 206);
            textBox2.Name = "textBox2";
            textBox2.Font = new Font("", 17F);
            textBox2.Size = new Size(315, 15);
            textBox2.BorderStyle = BorderStyle.None;
            textBox2.TextAlign = HorizontalAlignment.Center;

            //3
            textBox3 = new TextBox();
            textBox3.Location = new Point(914, 250);
            textBox3.Name = "textBox3";
            textBox3.Font = new Font("", 17F);
            textBox3.Size = new Size(315, 15);
            textBox3.BorderStyle = BorderStyle.None;
            textBox3.TextAlign = HorizontalAlignment.Center;

            //4
            textBox4 = new TextBox();
            textBox4.Location = new Point(914, 295);
            textBox4.Name = "textBox4";
            textBox4.Font = new Font("", 17F);
            textBox4.Size = new Size(315, 15);
            textBox4.BorderStyle = BorderStyle.None;
            textBox4.TextAlign = HorizontalAlignment.Center;

            //5
            textBox5 = new TextBox();
            textBox5.Location = new Point(914, 340);
            textBox5.Name = "textBox5";
            textBox5.Font = new Font("", 17F);
            textBox5.Size = new Size(315, 15);
            textBox5.BorderStyle = BorderStyle.None;
            textBox5.TextAlign = HorizontalAlignment.Center;


            //6
            textBox6 = new TextBox();
            textBox6.Location = new Point(914, 385);
            textBox6.Name = "textBox6";
            textBox6.Font = new Font("", 17F);
            textBox6.Size = new Size(315, 15);
            textBox6.BorderStyle = BorderStyle.None;
            textBox6.TextAlign = HorizontalAlignment.Center;

            //7
            textBox7 = new TextBox();
            textBox7.Location = new Point(914, 430);
            textBox7.Name = "textBox7";
            textBox7.Font = new Font("", 17F);
            textBox7.Size = new Size(315, 15);
            textBox7.BorderStyle = BorderStyle.None;
            textBox7.TextAlign = HorizontalAlignment.Center;

            //8
            textBox8 = new TextBox();
            textBox8.Location = new Point(914, 475);
            textBox8.Name = "textBox8";
            textBox8.Font = new Font("", 17F);
            textBox8.Size = new Size(315, 15);
            textBox8.BorderStyle = BorderStyle.None;
            textBox8.TextAlign = HorizontalAlignment.Center;

            //9
            textBox9 = new TextBox();
            textBox9.Location = new Point(766, 505);
            textBox9.Name = "textBox9";
            textBox9.Font = new Font("", 17F);
            textBox9.Size = new Size(315, 15);
            textBox9.BorderStyle = BorderStyle.None;
            textBox9.TextAlign = HorizontalAlignment.Center;

            //10
            textBox10 = new TextBox();
            textBox10.Location = new Point(966, 505);
            textBox10.Name = "textBox10";
            textBox10.Font = new Font("", 17F);
            textBox10.Size = new Size(250, 15);
            textBox10.BorderStyle = BorderStyle.None;
            textBox10.TextAlign = HorizontalAlignment.Center;
            mw.ResumeLayout();//это очень нужно 

            FillAnswerTable();//а это для проверки ответов

        }

        //проверка ответов
        void FillAnswerTable()
        {
            if (answers.Count != 0)
            {
                answers.Clear();
            }

            answers.Add(new Answer(textBox1, "reseption"));
            answers.Add(new Answer(textBox2, "reseptionist"));
            answers.Add(new Answer(textBox3, "key"));
            answers.Add(new Answer(textBox4, "number"));
            answers.Add(new Answer(textBox5, "form"));
            answers.Add(new Answer(textBox6, "bill"));
            answers.Add(new Answer(textBox7, "lift"));
            answers.Add(new Answer(textBox8, "floor"));



        }

        //отрисовка элементов в окне
        public void AddElements()
        {
            mw.Controls.Add(textBox1);
            mw.Controls.Add(textBox2);
            mw.Controls.Add(textBox3);
            mw.Controls.Add(textBox4);
            mw.Controls.Add(textBox5);
            mw.Controls.Add(textBox6);
            mw.Controls.Add(textBox7);
            mw.Controls.Add(textBox8);
            //mw.Controls.Add(textBox9);
           // mw.Controls.Add(textBox10);
            mw.Controls.Add(next);
            mw.Controls.Add(listen);
            mw.Controls.Add(menu);
            mw.Controls.Add(check);
            mw.Controls.Add(listening1);

        }

        //удаляет элементы
        public void RemoveElements()
        {
            mw.Controls.Remove(textBox1);
            mw.Controls.Remove(textBox2);
            mw.Controls.Remove(textBox3);
            mw.Controls.Remove(textBox4);
            mw.Controls.Remove(textBox5);
            mw.Controls.Remove(textBox6);
            mw.Controls.Remove(textBox7);
            mw.Controls.Remove(textBox8);
           // mw.Controls.Remove(textBox9);
           // mw.Controls.Remove(textBox10);
            mw.Controls.Remove(next);
            mw.Controls.Remove(check);
            mw.Controls.Remove(listen);
            mw.Controls.Remove(menu);
            mw.Controls.Remove(check);
            mw.Controls.Remove(listening1);
        }

        //событие по нажатию кнопки menu
        void menu_click(object sender, EventArgs e)
        {
            bl4_exes bl4vcex1 = new bl4_exes(mw);
            RemoveElements();
            bl4vcex1.AddElements();
            player.Stop();
        }


        //загружает аудио
        System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"media\bl4\bl4_write_ex1.wav");



        void listen_click(object sender, EventArgs e)
        {
            //тут заглушка вместо диалога

            //прикрутить таймер надо для текста кнопки, чтобы listen на stop менялось, а то я так и не понял

            player.Play();//проигрывать аудио

        }

        void next_click(object sender, EventArgs e)
        {
            bl4_exes bl4vcex = new bl4_exes(mw);//создаем локальную переменную в классе для обращения к другому классу
            RemoveElements();//убрать элементы
            bl4vcex.AddElements();//добавить элементы из другого класса через локальную переменную
            player.Stop();//остановка прослушивания
        }

        //это штука проверяет ответы по нажатию кнопки 
        void check_click(object sender, EventArgs e)
        {
            foreach (var a in answers)
            {
                if (a.element.Text != a.answer)
                {
                    a.element.ForeColor = Color.Red;
                }
                else
                {
                    a.element.ForeColor = Color.Green;
                }
            }
        }
    }
}