﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Text;

namespace SHRD
{
    public class options
    {
        PictureBox pictureBox1;
        Label label1;
        Button bt_exit;

        Form1 mw;
        FontLoader fl = new FontLoader();

        public options(Form1 form)
        {
            ////////////загрузка основного


            mw = form;

            mw.SuspendLayout();
            /////////////////////////////
            ///
        
            label1 = new Label();
            label1.Text = "Прога была сделана усилиями таких людей как:                            Настя Белокурова, Денис Лаухин, Женя Карелин, Сафронов Дмитрий Алексеевич и Гаврищук Ирина Александровна.";
            label1.Font = fl.Load() ;
            label1.Location = new Point(700, 100);
            label1.Size = new Size (400, 600);


            pictureBox1 = new PictureBox();
            pictureBox1.Image = Image.FromFile(@"media\bl3\bl3_spoke_ex2.wav");
            pictureBox1.Location = new Point(0, 0);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.TabIndex = 0;
            pictureBox1.TabStop = false;
            pictureBox1.Visible = true;
            pictureBox1.Dock = DockStyle.Fill;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

            bt_exit = new Button();
            bt_exit.BackColor = Color.MediumSlateBlue;
            bt_exit.Font = fl.Load();
            bt_exit.Location = new Point(46, 370);
            bt_exit.Name = "bt_exit";
            bt_exit.Size = new Size(200, 68);
            bt_exit.TabIndex = 3;
            bt_exit.Text = "Exit";
            bt_exit.UseVisualStyleBackColor = false;
            bt_exit.Click += new EventHandler(this.Bt_exit_Click);

            mw.ResumeLayout(false);
        }

        public void AddElements()
        {
            // mw.Controls.Add(label1);
            mw.Controls.Add(pictureBox1);
            mw.Controls.Add(bt_exit);
            mw.Controls.Add(pictureBox1);
        }

        public void RemoveElements()
        {
            // mw.Controls.Remove(pictureBox1);
            mw.Controls.Remove(pictureBox1);
            mw.Controls.Remove(label1);
            mw.Controls.Remove(bt_exit);
        }

        private void Bt_exit_Click(object sender, EventArgs e)
        {
            MainMenu MM = new MainMenu(mw);
            RemoveElements();
            MM.AddElements();
        }
    }
}