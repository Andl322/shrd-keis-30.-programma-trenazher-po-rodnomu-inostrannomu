﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Text;
using System.Windows.Input;

namespace SHRD
{
    public class MainMenu
    {
        PictureBox pictureBox1;
        Button bt_MainMenu_start;
        Button bt_options;
        Button bt_exit;

        Form1 mw;
        FontLoader fl = new FontLoader();

        public MainMenu(Form1 form)
        {
            ////////////загрузка основного
          

            mw = form;

            mw.SuspendLayout();
            /////////////////////////////

            pictureBox1 = new PictureBox();
            pictureBox1.Image = global::SHRD.Properties.Resources.menu_new;
            pictureBox1.Location = new Point(0, 0);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.TabIndex = 0;
            pictureBox1.TabStop = false;
            pictureBox1.Visible = true;
            pictureBox1.Dock = DockStyle.Fill;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

            bt_MainMenu_start = new Button();
            bt_MainMenu_start.BackColor = Color.MediumSlateBlue;
            bt_MainMenu_start.Font = fl.Load();
            bt_MainMenu_start.Location = new Point(46, 254);
            bt_MainMenu_start.Name = "bt_MainMenu_start";
            bt_MainMenu_start.Size = new Size(605, 68);
            bt_MainMenu_start.TabIndex = 1;
            bt_MainMenu_start.Text = "Visiting a foreign country";
            bt_MainMenu_start.UseVisualStyleBackColor = false;
            bt_MainMenu_start.Click += new EventHandler(this.Bt_MainMenu_start_Click);


            bt_options = new Button();
            bt_options.BackColor = Color.MediumSlateBlue;
            bt_options.Font = fl.Load();
            bt_options.Location = new Point(1263, 676);
            bt_options.Name = "bt_options";
            bt_options.Size = new Size(605, 68);
            bt_options.TabIndex = 2;
            bt_options.Text = "Options";
            bt_options.UseVisualStyleBackColor = false;
            bt_options.Click += new EventHandler(this.Bt_options_Click);

            bt_exit = new Button();
            bt_exit.BackColor = Color.MediumSlateBlue;
            bt_exit.Font = fl.Load();
            bt_exit.Location = new Point(46, 336);
            bt_exit.Name = "bt_exit";
            bt_exit.Size = new Size(605, 68);
            bt_exit.TabIndex = 3;
            bt_exit.Text = "Exit";
            bt_exit.UseVisualStyleBackColor = false;
            bt_exit.Click += new EventHandler(this.Bt_exit_Click);

        

        mw.ResumeLayout(false);


        }


        public void AddElements()
        {
            mw.Controls.Add(bt_MainMenu_start);
            mw.Controls.Add(bt_options);
            mw.Controls.Add(bt_exit);
            mw.Controls.Add(pictureBox1);
        }

        public void RemoveElements()
        {
            mw.Controls.Remove(pictureBox1);
            mw.Controls.Remove(bt_MainMenu_start);
            mw.Controls.Remove(bt_options);
            mw.Controls.Remove(bt_exit);
        }



        private void Bt_MainMenu_start_Click(object sender, EventArgs e)
        {
            VisitCountry vc = new VisitCountry(mw);
            RemoveElements();
            vc.AddElements();

            
        }

        private void bt_mouseE(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            
            if (e.KeyCode == Keys.F1 )
            {
                options bl1exe = new options(mw);
                RemoveElements();
                bl1exe.AddElements();
            }
           
        }


        public void Bt_options_Click(object sender, EventArgs e)
        {
            options bl1exe = new options(mw);
            RemoveElements();
            bl1exe.AddElements();
            
        }

        private void Bt_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
