﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace SHRD
{
    public class VisitCountry
    {
        Button btn1;
        Button shrd;
        Button btn2;
        Button btn3;
        Button btn4;
        Button btn5;
        Button btn6;
        Button btn7;
        Button btn8;
        Button btn9;
        Button btn10;
        Button back;
        PictureBox menu2;

        FontLoader fl = new FontLoader();

        Form1 mw;
        public VisitCountry(Form1 form)
        {
            //основа
            mw = form;

            mw.SuspendLayout();

            
            /////

            //countries
            btn1 = new Button();
            btn1.Location = new Point(330, 145);
            btn1.Size = new Size(605, 64);
            btn1.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn1.Font = fl.Load();
            btn1.Text = "Countries and nationalities";
            btn1.Click += new EventHandler(btn1_click);

            //we arrive
            btn2 = new Button();
            btn2.Location = new Point(330, 245);
            btn2.Size = new Size(605, 64);
            btn2.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn2.Font = fl.Load();
            btn2.Text = "We arrive to the country";
            btn2.Click += new EventHandler(btn2_click);

            //meeting
            btn3 = new Button();
            btn3.Location = new Point(330, 345);
            btn3.Size = new Size(605, 64);
            btn3.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn3.Font = fl.Load();
            btn3.Text = "Asking questions about transport";
            btn3.Click += new EventHandler(btn3_click);

            //airport
            btn4 = new Button();
            btn4.Location = new Point(330, 445);
            btn4.Size = new Size(605, 64);
            btn4.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn4.Font = fl.Load();
            btn4.Text = "Staying in a hotel";
            btn4.Click += new EventHandler(btn4_click);

            //hotel
            btn5 = new Button();
            btn5.Location = new Point(16, 511);
            btn5.Size = new Size(605, 64);
            btn5.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn5.Font = fl.Load();
            btn5.Text = "At the hotel";

            //City transport
            btn6 = new Button();
            btn6.Location = new Point(640, 115);
            btn6.Size = new Size(605, 64);
            btn6.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn6.Font = fl.Load();
            btn6.Text = "City transport";

            //money
            btn7 = new Button();
            btn7.Location = new Point(640, 214);
            btn7.Size = new Size(605, 64);
            btn7.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn7.Font = fl.Load();
            btn7.Text = "Money";

            //shop
            btn8 = new Button();
            btn8.Location = new Point(640, 308);
            btn8.Size = new Size(605, 64);
            btn8.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn8.Font = fl.Load();
            btn8.Text = "Shopping";

            //restaran
            btn9 = new Button();
            btn9.Location = new Point(640, 408);
            btn9.Size = new Size(605, 64);
            btn9.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn9.Font = fl.Load();
            btn9.Text = "At the restaurant";

            //sightseeing
            btn10 = new Button();
            btn10.Location = new Point(640, 511);
            btn10.Size = new Size(605, 64);
            btn10.BackColor = System.Drawing.Color.MediumSlateBlue;
            btn10.Font = fl.Load();
            btn10.Text = "Sightseeing";

            //back
            back = new Button();
            back.Location = new Point(47, 590);
            back.Size = new Size(210, 66);
            back.BackColor = System.Drawing.Color.FromArgb(61, 155, 153);
            back.Font = fl.Load();
            back.Text = "<<<Back";
            back.Click += new EventHandler(back_Click);
            
            shrd = new Button();
            shrd.Location = new Point(0, 201);
            shrd.Size = new Size(200, 200);


            menu2 = new PictureBox();
            menu2.Location = new Point(0, 0);
            menu2.Size = new Size(1904, 1045);
            menu2.Image = global::SHRD.Properties.Resources.visit_foreign_country;
            menu2.Dock = DockStyle.Fill;
            menu2.SizeMode = PictureBoxSizeMode.StretchImage;

            //это тоже основа
            mw.ResumeLayout();
            /////
        }

        public void AddElements()
        {
            mw.Controls.Add(btn1);
            mw.Controls.Add(btn2);
            mw.Controls.Add(btn3);
            mw.Controls.Add(btn4);
            /*mw.Controls.Add(btn5);
            mw.Controls.Add(btn6);
            mw.Controls.Add(btn7);
            mw.Controls.Add(btn8);
            mw.Controls.Add(btn9);
            mw.Controls.Add(btn10);*/
            mw.Controls.Add(back);
            mw.Controls.Add(menu2);

        }

        public void RemoveElements()
        {
            mw.Controls.Remove(btn1);
            mw.Controls.Remove(btn2);
            mw.Controls.Remove(btn3);
            mw.Controls.Remove(btn4);
            /*mw.Controls.Remove(btn5);
            mw.Controls.Remove(btn6);
            mw.Controls.Remove(btn7);
            mw.Controls.Remove(btn8);
            mw.Controls.Remove(btn9);
            mw.Controls.Remove(btn10)*/;
            mw.Controls.Remove(back);
            mw.Controls.Remove(menu2);
        }

        void btn1_click(object sender, EventArgs e)
        {
            Countries tm = new Countries(mw);
            RemoveElements();
            tm.AddElements();
        }

        void btn2_click(object sender, EventArgs e)
        {
            bl2_exes tm = new bl2_exes(mw);
            RemoveElements();
            tm.AddElements();
        }

        void btn3_click(object sender, EventArgs e)
        {
            bl3_exes tm = new bl3_exes(mw);
            RemoveElements();
            tm.AddElements();

        }
        void btn4_click (object sender, EventArgs e )
        {
            bl4_exes tm = new bl4_exes(mw);
            RemoveElements();
            tm.AddElements();
        }

        void back_Click(object sender, EventArgs e)
        {
            MainMenu mm = new MainMenu(mw);
            RemoveElements();
            mm.AddElements();
        }
    }
}
